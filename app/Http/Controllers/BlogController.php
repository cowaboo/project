<?php

namespace App\Http\Controllers;

use View;
use \Cowaboo\Models\Dictionary;

class BlogController extends Controller {
	function index($blogId = "Stadja's place") {
		$dictionary = Dictionary::getCurrentFromId($blogId);
		$entries = $dictionary->entry_objects;
		$title = $entries['title'];
		$subtitle = $entries['subtitle'];
		$footer = $entries['footer'];

		$blog = $entries['blog'];
		$posts = array($blog);
		while ($blog->previous) {
			$blog = $blog->previous_object;
			if ($blog->tags == '||blog||') {
				$posts[] = $blog;
			}
		}

		// dd($blog, $posts);

		return View::make('blog/index', compact('dictionary', 'posts', 'title', 'subtitle', 'footer'));
	}
}

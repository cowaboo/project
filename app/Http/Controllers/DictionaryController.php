<?php

namespace App\Http\Controllers;

use App\Template;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;
use TagList;
use TemplateList;
use URL;
use View;
use \Cowaboo\Models\Dictionary;
use \Cowaboo\Models\Entry;

class DictionaryController extends Controller {
	function index() {
		$tagList = TagList::getCurrent();
		return view('dictionaries', compact('tagList'));
	}

	function rss($dictionary) {
		// create new feed
		$feedNews = \App::make("feed");
		$feedNews->title = 'Observatory ' . $dictionary->id . ' RSS';
		$feedNews->description = 'Observatory ' . $dictionary->id . ' News Feed.';
		$feedNews->logo = url("/img/logo.jpg");
		$feedNews->icon = url("/favicon.ico");

		$feedNews->link = URL::route('dictionary.view', $dictionary->id);
		// $feedNews ->lang = 'en';
		$feedNews->setShortening(true); // true or false
		$feedNews->setTextLimit(100); // maximum length of description text

		$lastDate = false;
		foreach ($dictionary->entry_objects as $entry) {
			$lastDate = $lastDate < $entry->getDateWithFormat('YmdHi') ? $entry->getDateWithFormat('YmdHi') : $lastDate;
			// set item's title, author, url, pubdate, description and content
			$feedNews->add($entry->tags, $entry->author, URL::route('entry.show', array($dictionary->id, $entry->hash)), $entry->getDateWithFormat('YmdHi'), $entry->value, $entry->value_html);
		}

		$feedNews->pubdate = Carbon::createFromFormat('YmdHi', $lastDate); // date of latest news
		// $feedNews->setDateFormat('carbon'); // 'datetime', 'timestamp' or 'carbon'

		return $feedNews->render('atom');
	}

	function newDictionary() {
		$templateList = TemplateList::get();
		return view('dictionaries.new', compact('templateList'));
	}

	function createNewDictionary(Request $request) {
		$input = $request->all();
		$id = trim($input['id']);
		if (!$id) {
			return view('dictionaries.new');
		}
		Dictionary::createNew($id);

		if (isset($input['template']) && $input['template']) {
			$dictionary = Dictionary::getCurrentFromId($id);
			$template = Template::getCurrentFromId($input['template']);
			foreach ($template->entries as $key => $entryName) {
				$entry = new Entry();
				$entry->tags = $entryName;
				$entry->value = $entryName;
				$entry->save();
				$dictionary->addEntry($entry);
			}
			$dictionary->save();
		}

		return redirect(route('dictionary.view', $id));
	}

	function tagList() {
		return redirect(route('viewer.view', TagList::getCurrent()->hash));
	}

	function view($id) {
		$object = Dictionary::getCurrentFromId($id);
		$templateList = TemplateList::get();
		$corresponding_templates = $templateList->getCorrespondingFromDictionary($object);
		$current = true;
		return View::make('objects/dictionary-viewer', compact('object', 'current', 'corresponding_templates'));
	}

	function unsubscribe($dictionary) {

		if (!$dictionary->currentUserIsMember()) {
			Session::flash('error', 'You\'re not a member of this observatory');
			return redirect(route('dictionary.view', $dictionary->id));
		}

		$newDictionary = $dictionary->createNewVersion();

		$member_list = $newDictionary->member_list;
		$user = Auth::user();
		foreach ($member_list as $id => $email) {
			if ($email == $user->email) {
				unset($member_list[$id]);
			}
		}

		$newDictionary->member_list = $member_list;
		$newDictionary->save();
		Session::flash('success', 'You\'ve unsubscribe from the observatory');

		return redirect(route('dictionary.view', $dictionary->id));
	}
}

<?php

namespace App\Http\Controllers;

use View;
use \Cowaboo\Models\Entry;

class EntryController extends Controller {
	function index() {
		$entry = new Entry();
		$entry->tags = array('tag 1', 'tag 2', 'tag 3');
		$entry->value = "Bim bam boum";
		echo json_encode($entry);
	}

	function show($dictionaryId, $entry) {
		$object = $entry;
		$dictionary = $dictionaryId;
		return View::make('objects/entry-viewer', compact('dictionary', 'object'));
	}
}

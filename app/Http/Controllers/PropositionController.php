<?php

namespace App\Http\Controllers;

use App\Proposition;
use Auth;
use Illuminate\Http\Request;
use IPFS;
use Session;
use Stellar;
use Viewer;
use \Cowaboo\Models\Dictionary;
use \Cowaboo\Models\Entry;
use \Cowaboo\Models\User;

class PropositionController extends Controller {
	function index() {
		$proposition = new Proposition();
		echo json_encode($proposition);
	}

	function show($hash) {
		$ipfs = IPFS::get($hash);
		if (isset($ipfs->proposition) && isset($ipfs->proposition->value) && isset($ipfs->proposition->value->dictionary)) {
			$dictionary = Dictionary::getCurrentFromId($ipfs->proposition->value->dictionary);
			if ($dictionary->getConf('peerAcceptation')) {
				if ($ipfs->proposition->author == Auth::user()->email) {
					return redirect(route('proposition.confirmation', $hash));
				}
			}
		}
		return Viewer::view($hash);
	}

	function accept(Request $request, $proposition) {
		$input = $request->all();
		if (!isset($input['secretKey']) || !$input['secretKey']) {
			Session::flash('error', 'No secret key provided');
			return redirect(route('proposition.show', $proposition->hash));
		}

		$secretKey = $input['secretKey'];
		$userTest = User::retrieveByCredentials($secretKey);
		if ($userTest->email != Auth::user()->email) {
			Session::flash('error', 'Incorrect secret key !');
			return redirect(route('proposition.show', $proposition->hash));
		}

		if (isset($proposition->value->dictionary)) {
			$dictionary = Dictionary::getCurrentFromId($proposition->value->dictionary);
			if ($dictionary->getConf('peerAcceptation')) {
				$destination = User::retrieveById($proposition->author)->publicAddress;
				$public = Auth::user()->publicAddress;

				$balance = Stellar::getAccountBalance($public);
				if ($balance <= -10) {
					Session::flash('error', 'You can\'t accept the proposition because you don\'t have enough energy');
					return redirect(route('proposition.show', $proposition->hash));
				} else {
					$balance = Stellar::getAccountBalance($destination);
					if ($balance <= 10) {
						Stellar::getPaymentAccount($public, $secretKey, $destination, 1);
					}
				}
			}
		}

		return $proposition->accept();
	}

	function newMember($dictionary) {
		return view('propositions.newMember', compact('dictionary'));
	}
	function newEntry($dictionaryId) {
		$dictionary = Dictionary::getCurrentFromId($dictionaryId);
		return view('propositions.newEntry', compact('dictionary'));
	}
	function createNewEntry(Request $request) {
		$input = $request->all();

		$tags = $input['tags'];
		$tags = array_map(function ($entry) {
			return trim($entry);
		}, array_filter(explode(',', $tags)));

		$proposition = new Proposition();
		$entry = new Entry();
		$entry->tags = $tags;
		$entry->value = $input['value'];
		$proposition->value = new \stdClass;
		$proposition->remark = $input['remark'];
		$proposition->value->dictionary = $input['dictionaryId'];
		$proposition->value->entry = $entry;
		$proposition->type = 'newEntry';
		$hash = $proposition->save();
		return redirect(route('proposition.show', $hash));
	}
	function addNewMember(Request $request, $dictionary) {
		$input = $request->all();

		$proposition = new Proposition();
		$proposition->type = 'addNewMemberToDictionary';

		$proposition->value = new \stdClass;
		$proposition->value->dictionary = $dictionary->id;
		$proposition->remark = $input['remark'];
		$proposition->value->user = trim($input['email']);

		$hash = $proposition->save();

		if ($dictionary->getConf('peerAcceptation')) {
			return redirect(route('proposition.confirmation', $hash));
		}
		return redirect(route('proposition.show', $hash));
	}

	function modifyEntry(Request $request, $dictionary, $entry) {
		$input = $request->all();
		if (isset($input['value'])) {
			$entry->value = $input['value'];
		}
		return view('propositions.modifyEntry', compact('dictionary', 'entry'));
	}

	function modifyEntryTemp($dictionary, $entry) {
		return view('propositions.modifyEntry-temp', compact('dictionary', 'entry'));
	}

	function updateEntry(Request $request, $dictionary, $entry) {
		$input = $request->all();

		$proposition = new Proposition();
		$proposition->type = 'modifyEntry';

		$newEntry = new Entry();

		$tags = $input['tags'];
		$tags = array_map(function ($newEntry) {
			return trim($newEntry);
		}, array_filter(explode(',', $tags)));

		$newEntry->tags = $tags;
		$newEntry->value = $input['value'];
		$newEntry->previous = $entry->hash;

		$proposition->value = new \stdClass;
		$proposition->remark = $input['remark'];
		$proposition->value->dictionary = $dictionary->id;
		$proposition->value->oldEntry = $entry->hash;
		$proposition->value->newEntry = $newEntry;

		$hash = $proposition->save();

		if ($dictionary->getConf('peerAcceptation')) {
			return redirect(route('proposition.confirmation', $hash));
		}
		return redirect(route('proposition.show', $hash));
	}

	function modifyDictionaryConf(Request $request, $dictionary) {
		$input = $request->all();
		$confId = $input['confId'];
		$confValue = trim($input['confValue']);
		return view('propositions.modifyDictionaryConf', compact('dictionary', 'confId', 'confValue'));
	}

	function updateDictionaryConf(Request $request, $dictionary) {
		$input = $request->all();

		if (($input['confId'] == 'peerAcceptation') && $input['confValue'] && (sizeof($dictionary->member_list) < 2)) {
			Session::flash('error', 'You can\'t ask for peerAccept because your dictionary only has only 1 member !');
			return redirect(route('dictionary.view', $dictionary->id));
		}

		$proposition = new Proposition();
		$proposition->remark = $input['remark'];
		$proposition->value = new \stdClass;
		$proposition->value->dictionary = $input['dictionaryId'];
		$proposition->value->conf = new \stdClass;
		$proposition->value->conf->id = $input['confId'];
		$proposition->value->conf->value = $input['confValue'] ? $input['confValue'] : false;
		$proposition->type = 'modifyDictionaryConf';
		$hash = $proposition->save();

		if ($dictionary->getConf('peerAcceptation')) {
			return redirect(route('proposition.confirmation', $hash));
		}
		return redirect(route('proposition.show', $hash));
	}

	function modifyDictionaryCore(Request $request, $dictionary) {
		$input = $request->all();
		$coreId = $input['coreId'];
		return view('propositions.modifyDictionaryCore', compact('dictionary', 'coreId'));
	}

	function updateDictionaryCore(Request $request, $dictionary) {
		$input = $request->all();

		$proposition = new Proposition();
		$proposition->remark = $input['remark'];
		$proposition->value = new \stdClass;
		$proposition->value->dictionary = $input['dictionaryId'];
		$proposition->value->core = new \stdClass;
		$proposition->value->core->id = $input['coreId'];
		$proposition->value->core->value = $input['coreValue'] ? $input['coreValue'] : false;
		$proposition->type = 'modifyDictionaryCore';
		$hash = $proposition->save();

		if ($dictionary->getConf('peerAcceptation')) {
			return redirect(route('proposition.confirmation', $hash));
		}
		return redirect(route('proposition.show', $hash));
	}

	function confirmation($proposition) {
		if ($proposition->isAccepted()) {
			Session::flash('success', 'Proposition accepted !');
		} elseif ($proposition->isRejected()) {
			Session::flash('error', 'Proposition rejected !');
		} else {
			Session::flash('log', 'Waiting confirmation...');
		}

		return view('propositions.confirmation', compact('proposition'));
	}

}

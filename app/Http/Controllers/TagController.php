<?php

namespace App\Http\Controllers;

use TagList;
use \Cowaboo\Models\Dictionary;

class TagController extends Controller {
	function info($tag) {
		$tag = urldecode($tag);
		$dictionariesContainingTag = array();
		$tagList = TagList::getCurrent();
		foreach ($tagList->list as $dictionaryId => $tags) {
			if (strstr(strtolower($tags), '||' . strtolower($tag) . '||') !== FALSE) {
				$dictionariesContainingTag[$dictionaryId] = $tags;
			}
		}

		$linkedTags = array();
		$entriesContainingTag = array();
		foreach ($dictionariesContainingTag as $dictionaryId => $tags) {
			$dictionary = Dictionary::getCurrentFromId($dictionaryId);
			foreach ($dictionary->entries as $tags => $hash) {
				if (strstr(strtolower($tags), '||' . strtolower($tag) . '||') !== FALSE) {
					if (!isset($entriesContainingTag[$dictionaryId])) {
						$entriesContainingTag[$dictionaryId] = array();
					}
					$id = sizeof($entriesContainingTag[$dictionaryId]);
					$entriesContainingTag[$dictionaryId][$id] = new \stdClass();
					$entriesContainingTag[$dictionaryId][$id]->tags = $tags;
					$entriesContainingTag[$dictionaryId][$id]->hash = $hash;

					foreach (explode('||', $tags) as $linkedTag) {
						$linkedTag = strtolower($linkedTag);
						$linkedTag = trim($linkedTag);
						if (!$linkedTag || (strtolower($tag) == $linkedTag)) {
							continue;
						}
						if (!in_array($linkedTag, $linkedTags)) {
							$linkedTags[] = $linkedTag;
						}
					}
				}
			}
		}

		return view('objects/tag-viewer', compact('tag', 'linkedTags', 'dictionariesContainingTag', 'entriesContainingTag'));
	}
}

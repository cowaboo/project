<?php

namespace App\Http\Controllers;

use App\Template;
use Illuminate\Http\Request;
use Session;
use TemplateList;
use View;

class TemplateController extends Controller {
	function index() {
		$object = TemplateList::get();
		$current = true;
		return view('objects.template_list-viewer', compact('object', 'current'));
	}

	function show($template) {
		$object = $template;
		return view('objects.template-viewer', compact('object', 'current'));
	}

	function edit($template) {
		return view('templates.edit', compact('template'));
	}

	function create() {
		return view('templates/create');
	}

	function store(Request $request) {
		$input = $request->all();
		$template = new Template();
		$template->id = $input['id'];
		$template->entries = $input['entries'];
		$template->explaination = $input['explaination'];
		$template->applicationUrl = $input['applicationUrl'];
		$template->save();
		Session::flash('success', 'Template "' . $template->id . '" created');
		return redirect(route('template.show', $template->id));
	}

	function update(Request $request, $template) {
		$input = $request->all();
		$newTemplate = $template->createNewVersion();
		$template->id = $input['id'];
		$newTemplate->entries = $input['entries'];
		$newTemplate->applicationUrl = $input['applicationUrl'];
		$newTemplate->explaination = $input['explaination'];
		$newTemplate->save();
		Session::flash('success', 'Template "' . $template->id . '" modified');
		return redirect(route('template.show', $template->id));
	}
}

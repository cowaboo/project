<?php

namespace App\Http\Controllers;

use Auth;
use CowabooProject;
use Illuminate\Http\Request;
use Session;
use Stellar;
use UserList;
use View;

class UserController extends Controller {
	function login() {
		return view('users.login');
	}

	function balance() {
		return Stellar::getAccountBalance(Auth::user()->publicAddress);
	}

	function loginUser(Request $request) {
		$input = $request->all();
		if (!Auth::attempt(['secretKey' => $input['secretKey']], true)) {
			Session::flash('error', 'No user corresponding to this secret key');
			return redirect(route('login'));
		}

		Session::flash('success', 'Logged as ' . Auth::user()->email);

		return redirect()->intended('/');
	}

	function whoAmI() {
		dd(Auth::user());
	}

	function logout() {
		Auth::logout();
		Session::flash('success', 'Logged out');
		return redirect(route('home'));
	}

	function index() {
		$userList = UserList::get();
		return view('users', compact('userList'));
	}

	function create() {
		return view('users.create');
	}

	function registerUser(Request $request) {
		$input = $request->all();
		$email = strtolower($input['email']);
		$exist = UserList::userExists($email);

		if ($exist) {
			Session::flash('error', 'User ' . $email . 'already exists.');
			return redirect(route('login'));
		}

		$keyPair = Stellar::getNewKeyPair();

		$userListHash = UserList::addUser($email, $keyPair->public_address);

		if (Stellar::getInitAccount($keyPair->public_address, $keyPair->secret_key)) {
			$this->sendUserCreation($email, $keyPair);
		}

		return view('users.creation-accepted', compact('keyPair', 'email'));
	}

	protected function sendUserCreation($email, $keyPair) {
		// $mail =  "Hello !";
		// $mail .= "<br/><br/>You've been added to the CoWaBoo project.";
		// $mail .= "<br/>For this, we have created for you a Stellar account:";
		// $mail .= "<br/>- Public Address: ".$keyPair->public_address;
		// $mail .= "<br/>- Secret Key: ".$keyPair->secret_key;
		// $mail .= "<br/><strong>WARNING:</strong> DO NOT LOSE your secret key. It is really important, we won\'t be able to send it back to you later and you'll need it to make exchanges.";
		// $mail .= "<br/><br/>We know that sending this information by email is not a secured way, but it is still early beta. There are many other way for sending you this information (SMS, pidgeons, letters...) but right now it is the easiest way.";
		// $mail .= "<br/><br/>Anyway, we hop you'll enjoy playing with this project and start creating exchange of energy in your community.";
		// $mail .= "<br/><br/>Cheers,";
		// $mail .= "<br/>The CoWaBoo crew.";

		$mail = str_replace(
			array('%PUBLICADDRESS%', '%SECRETKEY%'),
			array($keyPair->public_address, $keyPair->secret_key),
			CowabooProject::findEntry('CoWaBoo,Welcome,email')->value_html
		);

		$to = $email;

		$subject = 'Welcome to the CoWaBoo project';

		$headers = "From: " . strip_tags('do-no-reply@cowaboo.net') . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

		return mail($to, $subject, $mail, $headers);
	}
}

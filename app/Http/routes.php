<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */
use \Cowaboo\Models\Dictionary;

Route::get('/temp',
	array('as' => 'blog.index', 'uses' => function () {
		$user_list = UserList::getCurrent();
		$new_user_list = $user_list->createNewVersion();
		$list = clone ($new_user_list->list);
		$id = "almiral.renie@gmail.com";
		unset($list->$id);
		$new_user_list->list = $list;
		// $new_user_list->save();
		dd($user_list->list, $new_user_list->list);
	}));

Route::get('/fr',
	array('as' => 'lang.french', 'uses' => function () {
		setcookie('lang', 'fr');
		return redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/');
	}));

Route::get('/en',
	array('as' => 'lang.english', 'uses' => function () {
		setcookie('lang', 'en');
		return redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/');
	}));

Route::get('/current/tag_list',
	array('as' => 'current.tag.list', 'uses' => function () {
		$tagList = TagList::getCurrent();
		return Redirect::route('ipfs.raw', array('hash' => $tagList->hash));
	}));

Route::get('/current/tag_list/hash',
	array('as' => 'current.tag.list.hash', 'uses' => function () {
		$tagList = TagList::getCurrent();
		return $tagList->hash;
	}));

Route::get('/current/user_list',
	array('as' => 'current.user.list', 'uses' => function () {
		$userList = UserList::getCurrent();
		return Redirect::route('ipfs.raw', array('hash' => $userList->hash));
	}));

Route::get('/current/user_list/hash',
	array('as' => 'current.user.list.hash', 'uses' => function () {
		$userList = UserList::getCurrent();
		return $userList->hash;
	}));

Route::get('/current/observatory/{dictionaryId}/hash',
	array('as' => 'current.dictionary.hash', 'uses' => function ($dictionary) {
		return $dictionary->hash;
	}));

Route::get('/current/observatory/{dictionaryId}',
	array('as' => 'current.dictionary', 'uses' => function ($dictionary) {
		return Redirect::route('ipfs.raw', array('hash' => $dictionary->hash));
	}));

Route::get('/ipfs/{hash}',
	array(
		'as' => 'ipfs.raw',
		'uses' => function ($hash) {
			return IPFS::getRaw($hash);
		},
	)
);

Route::get('/gateway/ipfs/{hash}', function ($hash) {
	return IPFS::getRaw($hash, true);
});

Route::get('/new',
	array('as' => 'blog.index', 'uses' => function () {
		return view('template-3-columns');
	}));

Route::get('/old', array('as' => 'old', 'uses' => function () {
	return view('welcome-old');
}));

Route::get('/user_guide', array('as' => 'user_guide', 'uses' => function () {
	return view('user_guide', array('cowabooProject' => Dictionary::getCurrentFromId('cowaboo_observatory_application')));
}));

Route::get('/about', array('as' => 'about', 'uses' => function () {
	return view('about');
}));

Route::get('/blog/demo/{blogId}',
	array('as' => 'blog.index', 'uses' => 'BlogController@index'));
Route::get('/blog/demo',
	array('as' => 'blog.index', 'uses' => 'BlogController@index'));

Route::get('/login',
	array('as' => 'login', 'uses' => 'UserController@login'));
Route::post('/login',
	array('as' => 'login', 'uses' => 'UserController@loginUser'));
Route::get('/logout',
	array('as' => 'logout', 'uses' => 'UserController@logout'));

Route::get('/user/create',
	array('as' => 'user.create', 'uses' => 'UserController@create'));

Route::post('/user/create',
	array('as' => 'user.create', 'uses' => 'UserController@registerUser'));

Route::get('/user/create/rejected/userAlreadyExists',
	array('as' => 'user.create.rejected.userAlreadyExists', 'uses' => function () {
		return view('users.creation-rejected', array('msg' => 'user already exists'));
	}));

Route::delete('/observatory/{dictionaryId}/unsubscribe',
	array('as' => 'dictionary.unsubscribe', 'uses' => 'DictionaryController@unsubscribe'));

Route::get('/observatory/{dictionaryId}/rss',
	array('as' => 'dictionary.rss', 'uses' => 'DictionaryController@rss'));

Route::group(['middleware' => 'auth'], function () {

	Route::get('/', array('as' => 'home', 'uses' => function () {
		$user = Auth::user();
		return view('welcome', array('tagList' => TagList::get(), 'templateList' => TemplateList::get()));
	}));

	Route::get('/view/{hash}', array('as' => 'viewer.view', 'uses' => function ($hash) {
		return Viewer::view($hash);
	}));

	Route::get('/tag/{tag}',
		array('as' => 'tag.info', 'uses' => 'TagController@info'));

	Route::get('/proposition/{hash}', array('as' => 'proposition.show', 'uses' => 'PropositionController@show'));

	// QmUG9H4RyWDCF1T7VMbJ7P816bvkb75Bcz2iDPyWkzrLK8
	Route::get('/entries',
		array('as' => 'entry.index', 'uses' => 'EntryController@index'));

	// QmV1FyNeApKyaCUSS4gThWP8eHaVT9ag2zaT2DQmgHms7d
	Route::get('/observatory/duplicate/{hash}',
		array('as' => 'dictionary.duplicate', 'uses' => 'DictionaryController@duplicate'));

	Route::get('/proposition',
		array('as' => 'proposition.index', 'uses' => 'PropositionController@index'));

	Route::get('/proposition/{proposition}/confirmation',
		array('as' => 'proposition.confirmation', 'uses' => 'PropositionController@confirmation'));

	Route::post('/proposition/{proposition}/accept',
		array('as' => 'proposition.accept', 'uses' => 'PropositionController@accept'));

	Route::post('/observatory/{dictionaryId}/{entry}/modify',
		array('as' => 'entry.modify', 'uses' => 'PropositionController@modifyEntry'));

	Route::get('/observatory/{dictionaryId}/{entry}/modify',
		array('as' => 'entry.modify', 'uses' => 'PropositionController@modifyEntry'));

	Route::put('/observatory/{dictionaryId}/{entry}/modify',
		array('as' => 'proposition.modifyEntry', 'uses' => 'PropositionController@updateEntry'));

	Route::get('/observatory/{dictionaryId}/modifyConf',
		array('as' => 'dictionary.modifyConf', 'uses' => 'PropositionController@modifyDictionaryConf'));
	Route::put('/observatory/{dictionaryId}/modifyConf',
		array('as' => 'dictionary.updateConf', 'uses' => 'PropositionController@updateDictionaryConf'));

	Route::get('/observatory/{dictionaryId}/modifyCore',
		array('as' => 'dictionary.modifyCore', 'uses' => 'PropositionController@modifyDictionaryCore'));
	Route::put('/observatory/{dictionaryId}/modifyCore',
		array('as' => 'dictionary.updateCore', 'uses' => 'PropositionController@updateDictionaryCore'));

	Route::get('/observatory/new',
		array('as' => 'dictionary.new', 'uses' => 'DictionaryController@newDictionary'));
	Route::post('/observatory/new',
		array('as' => 'dictionary.createNew', 'uses' => 'DictionaryController@createNewDictionary'));

	Route::get('/observatory/{id}',
		array('as' => 'dictionary.view', 'uses' => 'DictionaryController@view'));

	Route::get('/observatory/{dictionaryId}/{entry}',
		array('as' => 'entry.show', 'uses' => 'EntryController@show'));

	// QmNUSqeVRPXjt53wWjDavP2Eq6ASSk2bDuD9vHL8CJ2Pow
	Route::get('/observatory/{dictionaryId}/member/new',
		array('as' => 'dictionary.newMember', 'uses' => 'PropositionController@newMember'));
	Route::put('/observatory/{dictionaryId}/member/new',
		array('as' => 'proposition.newMember', 'uses' => 'PropositionController@addNewMember'));

	Route::get('/observatory/{id}/entry/new',
		array('as' => 'dictionary.newEntry', 'uses' => 'PropositionController@newEntry'));
	Route::post('/observatory/{id}/entry/new',
		array('as' => 'proposition.newEntry', 'uses' => 'PropositionController@createNewEntry'));

/*    Route::get('/observatory-temp/{dictionaryId}/{entry}/modify',
array('as' => 'entry.modify', 'uses' => 'PropositionController@modifyEntryTemp'));
 */

	Route::get('/tagList',
		array('as' => 'dictionary.tagList', 'uses' => 'DictionaryController@tagList'));

	// QmUG9H4RyWDCF1T7VMbJ7P816bvkb75Bcz2iDPyWkzrLK8
	Route::get('/observatories',
		array('as' => 'dictionary.index', 'uses' => 'DictionaryController@index'));

	Route::get('/templates',
		array('as' => 'template.index', 'uses' => 'TemplateController@index'));

	Route::get('/template/create',
		array('as' => 'template.create', 'uses' => 'TemplateController@create'));

	Route::post('/template/create',
		array('as' => 'template.store', 'uses' => 'TemplateController@store'));

	Route::get('/template/{templateId}',
		array('as' => 'template.show', 'uses' => 'TemplateController@show'));

	Route::get('/template/{templateId}/edit',
		array('as' => 'template.edit', 'uses' => 'TemplateController@edit'));

	Route::put('/template/{templateId}/edit',
		array('as' => 'template.update', 'uses' => 'TemplateController@update'));

	Route::get('/users',
		array('as' => 'user.index', 'uses' => 'UserController@index'));

	Route::get('/user/whoAmI',
		array('as' => 'user.whoAmI', 'uses' => 'UserController@whoAmI'));

	Route::get('/user/balance',
		array('as' => 'user.balance', 'uses' => 'UserController@balance'));
});

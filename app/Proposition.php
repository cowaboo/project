<?php

namespace App;

use Session;
use Storage;
use TagList;
use URL;
use \Cowaboo\Models\Dictionary;
use \Cowaboo\Models\Entry;
use \Cowaboo\Models\IPFSable;

class Proposition extends IPFSable {
	protected $keys = array('type', 'value', 'author', 'remark', 'date');
	protected $mainKey = 'proposition';

	static function createFromHash($hash) {
		$proposition = parent::createFromHash($hash);

		switch ($proposition->type) {
		case 'newEntry':
			$proposition->value->entry = Entry::createFromIpfs($proposition->value->entry);
			break;

		case 'modifyEntry':
			$proposition->value->oldEntry = Entry::createFromHash($proposition->value->oldEntry);
			$proposition->value->newEntry = Entry::createFromIpfs($proposition->value->newEntry);
			break;

		default:
			break;
		}
		return $proposition;
	}

	public function isRejected() {
		if (!Storage::disk('propositions')->has($this->hash)) {
			return false;
		}
		return Storage::disk('propositions')->get($this->hash) == 'rejected' ? true : false;
	}

	public function isAccepted() {
		if (!Storage::disk('propositions')->has($this->hash)) {
			return false;
		}
		return Storage::disk('propositions')->get($this->hash) == 'accepted' ? true : false;
	}

	public function accept() {
		switch ($this->type) {
		case 'modifyDictionaryConf':
			$dictionary = Dictionary::getCurrentFromId($this->value->dictionary);
			$newDictionary = $dictionary->createNewVersion();
			$conf = $newDictionary->conf;
			if (!is_array($conf)) {
				$conf = get_object_vars($conf);
			}
			$conf[$this->value->conf->id] = $this->value->conf->value;
			$newDictionary->conf = $conf;
			$hash = $newDictionary->save();
			Storage::disk('propositions')->put($this->hash, 'accepted');
			Session::flash('success', 'Conf "' . $this->value->conf->id . '" modified to "' . $this->value->conf->value . '"');
			$route = route('dictionary.view', $newDictionary->id);
			break;

		case 'modifyDictionaryCore':

			$coreId = $this->value->core->id;
			$coreValue = $this->value->core->value;

			if ($coreId == 'id') {
				$taglist = TagList::getCurrent();
				if (isset($taglist->list->$coreValue)) {
					Storage::disk('propositions')->put($this->hash, 'rejected');
					Session::flash('error', 'Observatory "' . $coreValue . '" already exists');
					return redirect(route('dictionary.view', $this->value->dictionary));
				}
			}

			$dictionaryId = $this->value->dictionary;
			$dictionary = Dictionary::getCurrentFromId($dictionaryId);
			$newDictionary = $dictionary->createNewVersion();

			$newDictionary->$coreId = $coreValue;
			$hash = $newDictionary->save();

			if ($this->value->dictionary != $newDictionary->id) {
				$taglist = TagList::getCurrent();
				$taglist->removeDictionaryId($this->value->dictionary);
			}

			Storage::disk('propositions')->put($this->hash, 'accepted');
			Session::flash('success', 'Core value "' . $this->value->core->id . '" modified to "' . $this->value->core->value . '"');
			$route = route('dictionary.view', $newDictionary->id);
			break;

		case 'newEntry':
			$dictionary = Dictionary::getCurrentFromId($this->value->dictionary);
			$newDictionary = $dictionary->createNewVersion();
			$entry = $this->value->entry;
			$entry->save();
			$newDictionary->addEntry($entry);
			$newDictionary->addUser($entry->author);
			$hash = $newDictionary->save();
			Storage::disk('propositions')->put($this->hash, 'accepted');
			Session::flash('success', 'New entry "' . $entry->tags . '" added');
			$route = route('entry.show', array($newDictionary->id, $entry->hash));
			break;

		case 'modifyEntry':
			$dictionary = Dictionary::getCurrentFromId($this->value->dictionary);
			$newDictionary = $dictionary->createNewVersion();
			$entry = $this->value->newEntry;
			$entry->save();

			$oldEntry = Entry::createFromHash($entry->previous);
			$newDictionary->removeEntry($oldEntry);
			$newDictionary->addEntry($entry);
			$newDictionary->addUser($entry->author);
			$hash = $newDictionary->save();
			Storage::disk('propositions')->put($this->hash, 'accepted');
			Session::flash('success', 'Entry "' . $entry->tags . '" modified');
			$route = route('entry.show', array($newDictionary->id, $entry->hash));
			break;

		case 'addNewMemberToDictionary':
			$dictionary = Dictionary::getCurrentFromId($this->value->dictionary);
			$newDictionary = $dictionary->createNewVersion();
			$newDictionary->addUser($this->value->user);
			$hash = $newDictionary->save();
			Storage::disk('propositions')->put($this->hash, 'accepted');
			Session::flash('success', 'New member "' . $this->value->user . '" added to observatory "' . $newDictionary->id . '"');
			$route = route('dictionary.view', $newDictionary->id);
			break;

		default:
			break;
		}

		return redirect($route);
		dd('accept', $this);
	}

	public function save(array $options = []) {
		$hash = parent::save($options);
		if (isset($this->value->dictionary)) {
			$dictionary = Dictionary::getCurrentFromId($this->value->dictionary);
			if ($dictionary->getConf('peerAcceptation')) {
				$this->sendAcceptationMail();
			}
		}
		return $this->hash;
	}

	protected function sendAcceptationMail() {
		$dictionary = Dictionary::getCurrentFromId($this->value->dictionary);
		$member_list = $dictionary->member_list;
		$authorEmail = $this->author;

		$member_list = array_filter($member_list, function ($memberEmail) use ($authorEmail) {
			return $memberEmail != $authorEmail;
		});

		$mail = "Hello !";
		$mail .= "<br/><br/>A member has posted a proposition in the '" . $dictionary->id . "' observatory.";
		$mail .= "<br/>For this proposition to be accepted, you can vote for it there:";
		$mail .= "<br/><a href='" . URL::route('viewer.view', $this->hash) . "'>" . URL::route('proposition.accept', $this->hash) . "</a>";
		$mail .= "<br/>";
		$mail .= "<br/><br/>Anyway, we hop you enjoy playing with this project and exchanging energy in your community.";
		$mail .= "<br/><br/>Cheers,";
		$mail .= "<br/>The CoWaBoo crew.";

		$to = implode(',', $member_list);

		$subject = "A new post in the '" . $dictionary->id . "' observatory";

		$headers = "From: " . strip_tags('do-no-reply@cowaboo.net') . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

		return mail($to, $subject, $mail, $headers);
	}
}

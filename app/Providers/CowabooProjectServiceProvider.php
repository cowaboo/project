<?php

namespace App\Providers;
use Illuminate\Support\ServiceProvider;
use \Cowaboo\Models\Dictionary;

class CowabooProjectServiceProvider extends ServiceProvider {
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot() {
		//
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register() {
		$this->app->singleton('CowabooProject', function ($app) {
			$cowabooProject = Dictionary::getCurrentFromId('cowaboo_observatory_application');
			return $cowabooProject;
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides() {
		return array('CowabooProject');
	}
}

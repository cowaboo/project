<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;
use \Cowaboo\Models\Dictionary;
use \Cowaboo\Models\Entry;

class RouteServiceProvider extends ServiceProvider {
	/**
	 * This namespace is applied to your controller routes.
	 *
	 * In addition, it is set as the URL generator's root namespace.
	 *
	 * @var string
	 */
	protected $namespace = 'App\Http\Controllers';

	/**
	 * Define your route model bindings, pattern filters, etc.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	public function boot(Router $router) {
		$router->bind('dictionary', function ($hash, $route) {
			$dictionary = Dictionary::createFromHash($hash);
			return $dictionary;
		});

		$router->bind('dictionaryId', function ($id, $route) {
			$dictionary = Dictionary::getCurrentFromId($id);
			return $dictionary;
		});

		$router->bind('templateId', function ($id, $route) {
			$template = \App\Template::getCurrentFromId($id);
			return $template;
		});

		$router->bind('proposition', function ($hash, $route) {
			$proposition = \App\Proposition::createFromHash($hash);
			return $proposition;
		});

		$router->bind('entry', function ($hash, $route) {
			$entry = Entry::createFromHash($hash);
			return $entry;
		});

		view()->composer('template-3-columns', function ($view) {
			$lang = isset($_COOKIE['lang']) ? $_COOKIE['lang'] : false;
			view()->share(
				array(
					'lang' => $lang,
				)
			);
		});

		parent::boot($router);
	}

	/**
	 * Define the routes for the application.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	public function map(Router $router) {
		$this->mapWebRoutes($router);

		//
	}

	/**
	 * Define the "web" routes for the application.
	 *
	 * These routes all receive session state, CSRF protection, etc.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	protected function mapWebRoutes(Router $router) {
		$router->group([
			'namespace' => $this->namespace, 'middleware' => 'web',
		], function ($router) {
			require app_path('Http/routes.php');
		});
	}
}

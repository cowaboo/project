<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class TemplateListServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('TemplateList', function ($app) {
            return \App\TemplateList::getCurrent();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array('TemplateList');
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('Viewer', function ($app) {
            return new \App\Services\Viewer\ViewerService();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array('Viewer');
    }
}

<?php
namespace App\Services\Viewer;
use App\Proposition;
use App\Template;
use IPFS;
use TagList;
use TemplateList;
use UserList;
use View;
use \Cowaboo\Models\Dictionary;
use \Cowaboo\Models\Entry;

/**
 *
 */
class ViewerService {
	function __construct() {
	}

	public function view($hash) {
		$ipfs = IPFS::get($hash);
		$type = IPFS::findType($ipfs);

		switch ($type) {
		case 'entry':
			$object = Entry::createFromHash($hash);
			break;

		case 'dictionary':
			$object = Dictionary::createFromHash($hash);
			break;

		case 'proposition':
			$object = Proposition::createFromHash($hash);
			break;

		case 'tag_list':
			$object = TagList::createFromHash($hash);
			break;

		case 'user_list':
			$object = UserList::createFromHash($hash);
			break;

		case 'template_list':
			$object = TemplateList::createFromHash($hash);
			break;

		case 'template':
			$object = Template::createFromHash($hash);
			break;

		default:
			$object = $ipfs;
			$type = 'standard';
			break;
		}

		return View::make('objects/' . $type . '-viewer', compact('object'));
	}

}

<?php

namespace App;

use \Cowaboo\Models\IPFSable;

class Template extends IPFSable {
	protected $keys = array('id', 'entries', 'explaination', 'applicationUrl', 'date', 'conf', 'previous', 'author');
	protected $mainKey = 'template';

	static function getCurrentFromId($id) {
		$templateList = \TemplateList::get();
		$hash = $templateList->list->$id;
		$template = self::createFromHash($hash);
		return $template;
	}

	public function save(array $options = []) {
		$hash = parent::save($options);
		$templateList = \TemplateList::get();
		$templateList->saveTemplate($this);
		return $hash;
	}

}

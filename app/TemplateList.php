<?php

namespace App;

use Storage;
use \Cowaboo\Models\IPFSable;

class TemplateList extends IPFSable {
	/**
	 * @var string
	 */
	static $fileName = 'templateList';

	/**
	 * @var array
	 */
	protected $keys = array('list', 'previous', 'date');

	/**
	 * @var string
	 */
	protected $mainKey = 'template_list';

	/**
	 * @return mixed
	 */
	public function get() {
		return $this;
	}

	/**
	 * @return mixed
	 */
	public static function getCurrent() {
		$current = new self();
		if (Storage::disk('shared')->has(self::$fileName)) {
			$hash = Storage::disk('shared')->get(self::$fileName);
			$current = self::createFromHash($hash);
		}
		return $current;
	}

	/**
	 * @param array $options
	 * @return mixed
	 */
	public function save(array $options = []) {
		$hash = parent::save($options);
		Storage::disk('shared')->put(self::$fileName, $hash);
		return $hash;
	}

	/**
	 * @param $template
	 * @return mixed
	 */
	public function saveTemplate($template) {
		$id = $template->id;
		$newTagList = $this->createNewVersion();
		$list = clone ($newTagList->list);
		$list->$id = $template->hash;
		$newTagList->list = $list;

		return $newTagList->save();
	}

	public function getCorrespondingFromDictionary($dictionary) {
		$correspondingTemplates = array();
		foreach ($this->list as $templateId => $templateHash) {
			$template = Template::createFromHash($templateHash);
			$correspond = true;
			foreach ($template->entries as $id => $entryName) {
				$title = '||' . $entryName . '||';
				$correspond = $correspond && isset($dictionary->entries->$title);
			}
			if ($correspond) {
				$correspondingTemplates[$template->id] = $template;
			}
		}
		return $correspondingTemplates;
	}
}

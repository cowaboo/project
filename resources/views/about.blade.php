@extends('template-3-columns')

@section('pageTitle')
    User guide
@endsection

@section('title')
    <span style="text-decoration: underline;">{{CowabooProject::findEntry('about_title')->value}}</span>
@endsection

@section('titleA')
{{CowabooProject::findEntry('about_left_title')->value}}
@endsection
@section('contentA')
{!!CowabooProject::findEntry('about_left_text')->value_html!!}
@endsection

@section('titleB')
{{CowabooProject::findEntry('about_center_title')->value}}
@endsection
@section('contentB')
{!!CowabooProject::findEntry('about_center_text')->value_html!!}
@endsection

@section('titleC')
{{CowabooProject::findEntry('about_right_title')->value}}
@endsection
@section('contentC')
    <div id="disqus_thread"></div>
@endsection

@push('scripts')
<script>
    /**
     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
     */
    
    var disqus_config = function () {
        this.page.url = "{{ route('home') }}";  // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = "home"; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };
    
    (function() {  // DON'T EDIT BELOW THIS LINE
        var d = document, s = d.createElement('script');
        
        s.src = '//cowaboo.disqus.com/embed.js';
        
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
@endpush
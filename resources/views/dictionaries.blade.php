@extends('template')

@section('pageTitle')
    Observatory list
@endsection

@section('title')
    <span style="text-decoration: underline;">Observatory list</span>:
@endsection

@section('content')
<p style="text-align: center;">
	<a href="{{route('dictionary.new')}}">+ create a new observatory</a>
</p>

<dl>
    @foreach ($tagList->list as $id => $tags)
      <dt><a href="{{route('dictionary.view', $id)}}">{{$id}}</a></dt>
      @if ($tags)
	      <dd>Tags: {{$tags}}</dd>
      @endif
    @endforeach
</dl>

<p>
    <a href="https://gateway.ipfs.io/ipfs/{{$tagList->hash}}" target="_blank">#</a>
</p>
@endsection
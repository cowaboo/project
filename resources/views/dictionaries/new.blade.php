@extends('template')

@section('pageTitle')
    New Dictionary creation
@endsection

@section('title')
    <span style="text-decoration: underline;">Creation of a new observatory</span>:
@endsection

@section('content')
	{!! Form::open(array('method' => 'POST', 'route' => array('dictionary.createNew'))) !!}
		<label for="id">Observatory identifier </label>
		<input type="text" name="id" id="id" value="" placeholder="exemple: cowaboo,science,..." style="width: 100%;" required>
		<br/><br/>
		<label for="id">Template to use </label><br/>
		<select name="template">
			<option value="">None</option>
			@foreach ($templateList->list as $templateId => $template)
				<option value="{{$templateId}}">{{$templateId}}</option>
			@endforeach
		</select>
		<br/><br/>
		<input type="submit" name="" value="Create dictionary !"/>
	{!! Form::close() !!}
@endsection
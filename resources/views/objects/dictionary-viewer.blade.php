@extends('template-3-columns')

@section('pageTitle')
    Observatory {{$object->id}}
@endsection

@section('title')
    Observatory:<br/>
    <a href="{{route('dictionary.view', $object->id)}}">{{$object->id}}</a>
    @if (isset($current) && $current)
        <small>(current&nbsp;version)</small>
    @else
        <small>(old&nbsp;version)</small>
    @endif
@endsection


@section('titleA')
    Select an entry
@endsection
@section('contentA')
    @include('partials/entryList', array('dictionary' => $object))
@endsection

@section('titleB')
    Add entry
@endsection
@section('contentB')
    @if (isset($current) && $current && $object->currentUserIsMember())
        <a href="{{route('dictionary.newEntry', $object->id)}}" class="pure-button pure-button-primary">+ Add Entry</a>
    @endif

@endsection

@section('titleC')
    Info
@endsection
@section('contentC')
    <p>
        <a href="{{ route('dictionary.rss', $object->id) }}" target="_blank">RSS</a>
        <br/>
        <a href="https://gateway.ipfs.io/ipfs/{{$object->hash}}" target="_blank">#</a>
        {{$object->date}},
        @if ($object->previous)
            <a href="{{route('viewer.view', $object->previous)}}">Previous version</a>
        @else
            First version
        @endif
    </p>
    @if (isset($current) && $current && $object->currentUserIsMember())
        <a class="pure-button button-secondary" href="{{ route('dictionary.modifyCore', array('dictionaryId' => $object->id, 'coreId' => 'id')) }}"><i class="fa fa-edit" aria-hidden="true"></i> change observatory name</a>
        <br/>
    @endif

    <h4>Member List</h4>
    @if (isset($current) && $current && !$object->currentUserIsMember())
        <a class="pure-button button-secondary" href="{{route('dictionary.newMember', $object->id)}}">+ Become a member</a>
    @endif
    <ul>
    @foreach ($object->memberList as $member)
        @if (isset($current) && $current && ($member == Auth::user()->email))
            <li>
                    {!! Form::open(array('method' => 'delete', 'route' => array('dictionary.unsubscribe', $object->id), 'style' => "display: inline; font-size: 0.5em;")) !!}
                        <button title="unsubscribe" class="pure-button button-error" type="submit" style="margin-top: -6px;"><i class="fa fa-minus"></i></button>
                    {!! Form::close() !!}&nbsp;&nbsp;{{$member}}
            </li>
        @else
            <li>{{$member}}</li>
        @endif
    @endforeach
    </ul>

    <p>
    <h4>You can propose rules for this group:</h4>
    <ul id="dictionaryRuleList">
        <li>
            @if ($object->getConf('peerAcceptation'))
                {!!CowabooProject::findEntry('observatory_conf_peerAcceptation_true')->value_html!!}
            @else
                {!!CowabooProject::findEntry('observatory_conf_peerAcceptation_false')->value_html!!}
            @endif
            @if ($object->currentUserIsMember())
                @if (isset($current) && $current)
                    {!! Form::open(array('method' => 'GET', 'route' => array('dictionary.modifyConf', $object->id))) !!}
                    @if ($object->getConf('peerAcceptation'))
                        <input type="hidden" name="confId" value="peerAcceptation"/>
                        <input type="hidden" name="confValue" value="0"/>
                        <button class="pure-button button-warning" type="submit"><i class="fa fa-user" aria-hidden="true"></i> change to self acceptation</button>
                    @else
                        <input type="hidden" name="confId" value="peerAcceptation"/>
                        <input type="hidden" name="confValue" value="1"/>
                        <button class="pure-button button-success" type="submit"><i class="fa fa-users" aria-hidden="true"></i> change to peer validation</button>
                    @endif
                    {!! Form::close() !!}
                @endif
            @endif
        </li>
        <li>
            @if ($object->getConf('private'))
               <span class="button-warning">The current group is Private</span>
            @else
               <span class="button-success">The current group is Public</span>
            @endif

            @if ($object->currentUserIsMember())
                @if (isset($current) && $current)
                    <br/>
                    {!! Form::open(array('method' => 'GET', 'route' => array('dictionary.modifyConf', $object->id))) !!}
                    @if ($object->getConf('private'))
                        <input type="hidden" name="confId" value="private"/>
                        <input type="hidden" name="confValue" value="0"/>
                        <button class="pure-button button-success" type="submit"><i class="fa fa-unlock" aria-hidden="true"></i> change to public</button>
                    @else
                        <input type="hidden" name="confId" value="private"/>
                        <input type="hidden" name="confValue" value="1"/>
                        <button class="pure-button button-warning" type="submit"><i class="fa fa-lock" aria-hidden="true"></i> change to private</button>
                    @endif
                    {!! Form::close() !!}
                @endif
            @endif
        </li>
    </ul>

    @if ($corresponding_templates)
        <p>

        <h4>Corresponding Templates:</h4>
        <ul>
        @foreach ($corresponding_templates as $templateId => $template)
            <li>
                {{$templateId}}:
                @if ($template->applicationUrl)
                    <a href="{{$template->applicationUrl.$object->id}}" target="_blank">lien</a>
                @endif
            </li>
        @endforeach
        </ul>
    </p>
    @endif

    <p>
    <h4>Current configuration</h4>
        <pre class="json">{{json_encode($object->conf)}}</pre>
    </p>

    <h4>Observatory discussion</h4>
    <div id="disqus_thread"></div>

@endsection

@push('scripts')
<script>
    /**
     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
     */

    var disqus_config = function () {
        this.page.url = "{{ route('dictionary.view', $object->id) }}";  // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = "dictionary_{{$object->id}}"; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };

    (function() {  // DON'T EDIT BELOW THIS LINE
        var d = document, s = d.createElement('script');

        s.src = '//cowaboo.disqus.com/embed.js';

        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
@endpush

@push('styles')
<style>
    .pure-menu-link, .pure-menu-heading {
        white-space: normal;
    }

    #dictionaryRuleList li {
        margin-bottom: 1em;
    }
</style>
@endpush

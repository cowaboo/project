@extends('template-3-columns')

@section('pageTitle')
    Entry {{$object->tags}} for observatory {{$dictionary->id}}
@endsection

@section('title')
    Entry:
    <br/>
    <small><span style="border: 1px solid; padding: 5px;">
        @if($dictionary->getCurrentEntryHash($object))
            <a href="{{route('entry.show', array($dictionary->id, $dictionary->getCurrentEntryHash($object)))}}">{{$object->tags}}</a>
        @else
            {{$object->tags}}
        @endif
    </span>
    @if ($dictionary->isCurrentEntry($object))
        (current&nbsp;version)
    @else
        (old&nbsp;version)
    @endif
    </small>
@endsection

@section('titleA')
    Select an entry
@endsection
@section('contentA')
    @include('partials/entryList', array('dictionary' => $dictionary, 'currentEntry' => $object))
@endsection

@section('titleB')
    Entry
@endsection

@section('contentB')

@if ($dictionary->isCurrentEntry($object) && $dictionary->currentUserIsMember())

    {!! Form::open(array('method' => 'POST', 'route' => array('proposition.modifyEntry', $dictionary->id, $object->hash), 'class' => "pure-form")) !!}
        <textarea class="markdown" id="value" name="value">{{$object->value}}</textarea>
        <button class="pure-button pure-button-primary">Propose your entry modifications</button>
    {!! Form::close() !!}

@else

<p class="markdown">
{{$object->value}}
</p>

@endif


@endsection

@section('titleC')
    Action
@endsection
@section('contentC')
<p>
        <a href="{{ route('dictionary.rss', $dictionary->id) }}" target="_blank">RSS</a>
        <br/>
    <a href="https://gateway.ipfs.io/ipfs/{{$object->hash}}" target="_blank">#</a>
    {{$object->date}},
    {{$object->author}},
@if ($object->previous)
    <a href="{{route('entry.show', array($dictionary->id, $object->previous))}}">Previous version</a>
@else
First version
@endif
</p>
 <p>
    Observatory: <a href="{{route('dictionary.view', $dictionary->id)}}">{{$dictionary->id}}</a><br/>
    @if (!$dictionary->currentUserIsMember())
        <a class="pure-button button-secondary" href="{{route('dictionary.newMember', $dictionary->id)}}">+ Become a member</a>
    @endif
    <hr/>
</p>
    Observatory discussion
    <div id="disqus_thread"></div>
@endsection

@push('scripts')
<script>
    /**
     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
     */

    var disqus_config = function () {
        this.page.url = "{{ route('dictionary.view', $dictionary->id) }}";  // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = "dictionary_{{$dictionary->id}}"; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };

    (function() {  // DON'T EDIT BELOW THIS LINE
        var d = document, s = d.createElement('script');

        s.src = '//cowaboo.disqus.com/embed.js';

        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
@endpush

@push('styles')
<style>
    .pure-menu-link, .pure-menu-heading {
        white-space: normal;
    }

    .pure-menu-selected>.pure-menu-link {
        background-color: #eee;
    }
</style>
@endpush

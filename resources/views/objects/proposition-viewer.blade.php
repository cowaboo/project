@extends('template-3-columns')

@section('pageTitle')
    Proposition
@endsection

@section('title')
    Proposition
    <small>(
        @if ($object->isAccepted())
            Accepted
        @elseif ($object->isRejected())
            Rejected
        @else
            Pending
        @endif
        )</small>
@endsection

@section('titleA')
Proposition Info
@endsection
@section('contentA')
<p>
    Type: {{$object->type}} <a href="https://gateway.ipfs.io/ipfs/{{$object->hash}}" target="_blank">#</a>
</p>

<p>
    {{$object->date}},
    {{$object->author}}
</p>
@endsection


@section('titleB')
Proposition value
@endsection
@section('contentB')

    @if ($object->type == 'newEntry')
        <p>Goal: <strong style="text-decoration: underline; font-size: larger;">Add a new entry</strong></p>
        <p>
        Value:
            <ul>
                <li>Observatory: <a href="{{route('dictionary.view', $object->value->dictionary)}}" target="_blank">{{$object->value->dictionary}}</a></li>
                <li>Tags: {{$object->value->entry->tags}}</li>
                <li>Value: <p class="markdown">{{$object->value->entry->value}}</p></li>
                <li>remark: <pre><code>{{$object->remark}}</code></pre></li>
                <li>author: {{$object->value->entry->author}}</li>
                <li>Date: {{$object->value->entry->date}}</li>
            </ul>
        </p>
    @elseif ($object->type == 'modifyEntry')
        <p>Goal: <strong style="text-decoration: underline; font-size: larger;">Modify an entry</strong></p>
        <p>
        <p>Observatory: <a href="{{route('dictionary.view', $object->value->dictionary)}}" target="_blank">{{$object->value->dictionary}}</a></p>
        New Value:
            <ul>
                <li>Tags: {{$object->value->newEntry->tags}}</li>
                <li>Entry: <p class="markdown">{{$object->value->newEntry->value}}</p></li>
                <li>remark: <pre><code>{{$object->remark}}</code></pre></li>
                <li>author: {{$object->value->newEntry->author}}</li>
                <li>Date: {{$object->value->newEntry->date}}</li>
            </ul>
            <br/>
        Old Value:
            <ul>
                <li>Tags: {{$object->value->oldEntry->tags}}</li>
                <li>Entry: <p class="markdown">{{$object->value->oldEntry->value}}</p></li>
                <li>author: {{$object->value->oldEntry->author}}</li>
                <li>Date: {{$object->value->oldEntry->date}}</li>
                @if ($object->value->oldEntry->previous)
                    <li>Previous: {{$object->value->oldEntry->previous}}</li>
                @else
                    <li>First version</li>
                @endif
                <li>Hash: <a href="https://gateway.ipfs.io/ipfs/{{$object->value->oldEntry->hash}}" target="_blank">{{$object->value->oldEntry->hash}}</a></li>
            </ul>
        </p>
    @elseif ($object->type == 'addNewMemberToDictionary')
        <p>Goal: <strong style="text-decoration: underline; font-size: larger;">Add a new member to the observatory</strong></p>
        <p>Observatory: <a href="{{route('dictionary.view', $object->value->dictionary)}}" target="_blank">{{$object->value->dictionary}}</a></p>
        <p>User: {{$object->value->user}}</p>
    @elseif ($object->type == 'modifyDictionaryConf')
        <p>Goal: <strong style="text-decoration: underline; font-size: larger;">Change dictionary configuration</strong></p>
        <p>Observatory: <a href="{{route('dictionary.view', $object->value->dictionary)}}" target="_blank">{{$object->value->dictionary}}</a></p>
        <p>
            <ul>
                <li>Configuration id: <strong>{{$object->value->conf->id}}</strong></li>
                <li>Configuration value proposition: <strong>{{$object->value->conf->value ? $object->value->conf->value : '0' }}</strong></li>
            </ul>
        </p>
        <p>Remark: {{$object->remark}}</p>

    @elseif ($object->type == 'modifyDictionaryCore')
        <p>Goal: <strong style="text-decoration: underline; font-size: larger;">Change dictionary core value</strong></p>
        <p>Observatory: <a href="{{route('dictionary.view', $object->value->dictionary)}}" target="_blank">{{$object->value->dictionary}}</a></p>
        <p>
            <ul>
                <li>Core value id: <strong>{{$object->value->core->id}}</strong></li>
                <li>Core value proposition: <strong>{{$object->value->core->value ? $object->value->core->value : '0' }}</strong></li>
            </ul>
        </p>
        <p>Remark: {{$object->remark}}</p>

        
    @else
        <pre class="json">{{json_encode($object->value)}}</pre>
    @endif
@endsection

@section('titleC')
Action
@endsection
@section('contentC')
@if (!$object->isRejected())
    {!! Form::open(array('method' => 'POST', 'route' => array('proposition.accept', $object->hash))) !!}
        <label>Your secret key
            <br/>
        <input type="password" name="secretKey" style="width: 100%;" required/>
        </label>
        <br/><br/>
        <input type="submit" name="" value="Accept the proposition"/>
    {!! Form::close() !!}
    <br/>
    Proposition discussion
    <div id="disqus_thread"></div>
@endif
@endsection

@push('scripts')
<script>
    /**
     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
     */
    
    var disqus_config = function () {
        this.page.url = "{{ route('proposition.show', $object->hash) }}";  // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = "proposition_{{$object->hash}}"; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };
    
    (function() {  // DON'T EDIT BELOW THIS LINE
        var d = document, s = d.createElement('script');
        
        s.src = '//cowaboo.disqus.com/embed.js';
        
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
@endpush
@extends('template-3-columns')

@section('pageTitle')
    Tag "{{$tag}}"
@endsection

@section('title')
    Tag "{{$tag}}"
@endsection

@section('titleA')
    Linked Tags
@endsection
@section('contentA')
    <ul>
        @foreach ($linkedTags as $linkedTag)
            <li class="tagFather">
                <span class="tag button-neutral" data-tag="{{strtolower($linkedTag)}}"><a href="{{ route('tag.info', urlencode(strtolower($linkedTag))) }}">{{$linkedTag}}</a></span>
            </li>
        @endforeach
    </ul>
@endsection

@section('titleB')
    Entries using the tag
@endsection
@section('contentB')
<dl>
    @foreach ($entriesContainingTag as $dictionaryId => $entries)
      <dl>
        <dt>{{$dictionaryId}}</dt>
        <dl>
            <ul>
                @foreach ($entries as $entry)
                    <li class="tagFather entry">
                    @foreach (array_filter(array_unique(explode('||', $entry->tags))) as $entryTag)
                        <span class="tag button-neutral" data-tag="{{strtolower($entryTag)}}">
                    @endforeach
                    
                    <a href="{{ route('entry.show', array($dictionaryId, $entry->hash)) }}">{{$entry->tags}}</a>
                    
                    @foreach (array_filter(array_unique(explode('||', $entry->tags))) as $entryTag)
                        </span> 
                    @endforeach
                    </li>
                @endforeach
            </ul>
        </dl>
      </dl>
    @endforeach
</dl>
@endsection

@section('titleC')
    Observatory using the tag
@endsection
@section('contentC')
<dl>
    @foreach ($dictionariesContainingTag as $id => $tags)
      <li><dt><strong><a href="{{route('dictionary.view', $id)}}">{{$id}}</a></strong></dt>
@if ($tags)
          <dd>
          || 
          @foreach (array_filter(array_unique(explode('||', $tags))) as $tag)
                <span class="tagFather">
                    <span class="tag button-neutral" data-tag="{{strtolower($tag)}}"><a href="{{ route('tag.info', urlencode(strtolower($tag))) }}">{{$tag}}</a></span>
                </span> ||
          @endforeach
          </dd>
      @endif</li>
    @endforeach
</dl>
@endsection

@push('scripts')
<script>
    $('.tag').hover(function(ev) {
        $('.tag[data-tag="'+this.dataset.tag+'"').addClass('button-secondary');
        $('.tag[data-tag="'+this.dataset.tag+'"').each(function(key, item) {
            $(item).find('.tag').addClass('button-secondary');
        });

    },function(ev) {
        $('.tag[data-tag="'+this.dataset.tag+'"').removeClass('button-secondary');
        $('.tag[data-tag="'+this.dataset.tag+'"').each(function(key, item) {
            $(item).find('.tag').removeClass('button-secondary');
        });
    });
</script>
@endpush

@push('styles')
<style>
    .tag a {
        text-decoration: none;
    }
    .entry {
        margin-bottom: 0.5em;
    }
</style>
@endpush



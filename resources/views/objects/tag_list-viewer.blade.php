@extends('objects/viewer')

@section('pageTitle')
    Tag List
@endsection

@section('title')
    <span style="text-decoration: underline;">Tag List</span>:
@endsection

@section('content')

<p>
    <ul>
    @foreach ($object->list as $id => $tags)
        <li><a href="{{route('dictionary.view', $id)}}" target="_blank">{{$id}}</a> - {{$tags}}</li>
    @endforeach
    </ul>
</p>

<p>
    <a href="https://gateway.ipfs.io/ipfs/{{$object->hash}}" target="_blank">#</a>
    {{$object->date}}
</p>

<p>
@if ($object->previous)
    <a href="{{route('viewer.view', $object->previous)}}">Previous version</a>
@else
First version
@endif
</p>

@endsection

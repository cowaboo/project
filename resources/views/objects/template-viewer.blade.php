@extends('objects/viewer')

@section('pageTitle')
    Template {{$object->id}}
@endsection

@section('title')
    Template:
    <br/>
    <small>
        <span style="border: 1px solid; padding: 5px;">{{$object->id}}</span>
    </small>
@endsection

@section('content')

<p>
    Template: {{$object->id}}
    <hr/>
</p>

<p id="entry">
    Mandatory entry list:
    <ul>
        @foreach ($object->entries as $entry)
            <li>{{$entry}}</li>
        @endforeach
    </ul>
</p>

@if (isset($object->applicationUrl) && $object->applicationUrl)
<p>
    Application URL:
    <pre>
        <code>
{{$object->applicationUrl}}
        </code>
    </pre>
</p>
@endif

<p>
    Explaination:
    <pre>
        <code>
{{$object->explaination}}
        </code>
    </pre>
</p>

<p>
    <hr/>
	<a href="https://gateway.ipfs.io/ipfs/{{$object->hash}}" target="_blank">#</a>
    {{$object->date}},
    {{$object->author}}
     - <a href="{{ route('template.edit', $object->id) }}">Edit template</a>
</p>

<p>
@if ($object->previous)
    <a href="{{route('viewer.view', array($object->previous))}}">Previous version</a>
@else
First version
@endif
</p>

@endsection

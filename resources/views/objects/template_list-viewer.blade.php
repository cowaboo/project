@extends('objects/viewer')

@section('pageTitle')
    Template List
@endsection

@section('title')
    <a href="{{ route('template.index') }}">Template List</a>
    @if (isset($current) && $current)
        <small>(current)</small>
    @else
        <small>(old)</small>
    @endif
@endsection

@section('content')

<p>
    @if (isset($current) && $current)
        <a href="{{route('template.create')}}" target="_blank">+ Add a template</a>
    @endif
    <ul>
    @foreach ($object->list as $id => $hash)
        <li><a href="{{route('viewer.view', $hash)}}" target="_blank">{{$id}}</a> - {{$hash}}</li>
    @endforeach
    </ul>
</p>

<p>
    <a href="https://gateway.ipfs.io/ipfs/{{$object->hash}}" target="_blank">#</a>
    {{$object->date}}
</p>

<p>
@if ($object->previous)
    <a href="{{route('viewer.view', $object->previous)}}">Previous version</a>
@else
First version
@endif
</p>

@endsection

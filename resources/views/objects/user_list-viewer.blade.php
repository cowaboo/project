@extends('objects/viewer')

@section('pageTitle')
    User List
@endsection

@section('title')
    <span style="text-decoration: underline;"><a href="{{route('user.index')}}">User List</a></span> <small>(old&nbsp;version)</small>
@endsection

@section('content')

<p>
    <ul>
    @foreach ($object->list as $email => $publicAddress)
        <li>{{$email}} - {{$publicAddress}}</li>
    @endforeach
    </ul>
</p>

<p>
    <a href="https://gateway.ipfs.io/ipfs/{{$object->hash}}" target="_blank">#</a>
    {{$object->date}}
</p>

<p>
@if ($object->previous)
    <a href="{{route('viewer.view', $object->previous)}}">Previous version</a>
@else
First version
@endif
</p>

@endsection

<div class="pure-menu custom-restricted-width">
    <form id="entryList" class="pure-form" v-cloak>
        <input type="text" v-model="entryFilterString" id="entryFilterString" value="" placeholder="Enter your search terms" style="width: 100%;" required="">
        <ul class="pure-menu-list">
            <li v-for="e in filteredEntries" class="pure-menu-item" v-bind:class="{ 'pure-menu-selected': e.current}">
                <a class="pure-menu-link" v-bind:href="e.url">
                    @{{e.tags | html_entity_decode}}
                </a>
            </li>
        </ul>
    </form>
    @if ($dictionary->currentUserIsMember())
    <a href="{{route('dictionary.newEntry', $dictionary->id)}}" class="pure-button pure-button-primary">+ Add Entry</a>
    @endif
</div>


@push('scripts')
<script>

    var entries = [
    @foreach ($dictionary->entries_reverse as $tags => $hash)
    {
        "tags": "{{$tags}}",
        "url": "{{route('entry.show', array($dictionary->id, $hash))}}",
        "current": "{{(isset($currentEntry) && $currentEntry) && ($currentEntry->tags == $tags)}}"
    },
    @endforeach
    ];

    var entryFilter = new Vue({
        el: '#entryList',
        data: {
            entryFilterString: "",
            entries: entries,
            filteredEntries: entries,
        },
        filters: {
            html_entity_decode: function (value) {
                if (!value) return ''
                value = value.toString()
                return $('<textarea />').html(value).text();
            }
        },
        watch: {
            entryFilterString: function (newEntryFilterString) {
                this.filteredEntries = this.filterEntries(this.entries, newEntryFilterString);
            }
        },
        methods: {
            filterEntries: function(entries, entryFilterString) {
               if(!entryFilterString){
                return entries;
            }
            entryFilterString = no_accent(entryFilterString.trim().toLowerCase());
            result = entries.filter(function(item){
                item.tags = $('<textarea />').html(item.tags).text();
                console.log(no_accent(item.tags.toLowerCase()), no_accent(entryFilterString.trim().toLowerCase()));
                if(no_accent(item.tags.toLowerCase()).indexOf(entryFilterString) !== -1){
                    return item;
                }
            })
            return result;
        }
    }
});
</script>
@endpush

@extends('template-3-columns')

@section('pageTitle')
    Proposition confirmation
@endsection

@section('title')
    <span style="text-decoration: underline;">Confirmation of your proposition creation</span>:
@endsection

@section('titleA')
Info
@endsection
@section('contentA')
The proposition
<pre class="json">{{json_encode($proposition)}}</pre> 
@endsection

@section('titleB')
	Congratulation
@endsection
@section('contentB')
	<p>
	    You have created a proposition.<br/>
	    As your dictionary has the "peerAcceptation" configuration,<br/>
	    other members of the dictionary have to accept it.
	    <br/><br/>
	    They will receive a mail about it, shortly.
	</p>
@endsection

@section('titleC')
	Action
@endsection
@section('contentC')
	If you want to add info to your proposition, you can discuss it with voting members
	<br/>
    Proposition discussion
    <div id="disqus_thread"></div>
@endsection

@push('scripts')
<script>
    /**
     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
     */
    
    var disqus_config = function () {
        this.page.url = "{{ route('proposition.show', $proposition->hash) }}";  // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = "proposition_{{$proposition->hash}}"; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };
    
    (function() {  // DON'T EDIT BELOW THIS LINE
        var d = document, s = d.createElement('script');
        
        s.src = '//cowaboo.disqus.com/embed.js';
        
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
@endpush
@extends('template-3-columns')

@section('pageTitle')
    Proposing an observatory configuration modification
@endsection

@section('title')
    <span style="text-decoration: underline;">Proposing an observatory configuration modification</span>:
@endsection

@section('titleA')
Info
@endsection
@section('contentA')
You are creating a modification for the configuration of an observatory.<br/>
<br/>
Current version of observatory configuration: 
<pre class="json">{{json_encode($dictionary->conf)}}</pre> 
@endsection

@section('titleB')
	Configuration modification
@endsection
@section('contentB')
	<p>
	    <ul>
	    	<li>
	    		Observatory: <a href="{{route('dictionary.view', $dictionary->id)}}">{{$dictionary->id}}</a>
	    	</li>
	    </ul>
	</p>
	<hr/>	
	{!! Form::open(array('method' => 'PUT', 'route' => array('dictionary.updateConf', $dictionary->id))) !!}
		<input type="hidden" name="dictionaryId" value="{{$dictionary->id}}"/>
		
		<label for="confId">Configuration identifier</label><br/>
		<input type="text" name="confId" id="confId" style="" required value="{{$confId}}"/>
<br/><br/>
		<label for="confValue">Value proposition for this configuration</label><br/>
		<input type="text" name="confValue" id="confValue" style="" required value="{{$confValue}}"/>
@endsection

@section('titleC')
	Action
@endsection
@section('contentC')
	<div class="pure-form">
		<label for="remark">Why do you modify this observatory configuration ?</label>
		<textarea name="remark" id="remark" style="width: 100%; min-height: 10em;" required></textarea>
		<br/><br/>
		<input type="submit" name="" class="pure-button pure-button-primary" value="Create proposition !"/>
	</div>
	{!! Form::close() !!}
@endsection


@push('scripts')
<script>
</script>
@endpush

@push('styles')
	<style>
	
	</style>
@endpush
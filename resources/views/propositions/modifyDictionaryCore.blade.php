@extends('template-3-columns')

@section('pageTitle')
    Proposing an observatory core modification
@endsection

@section('title')
    <span style="text-decoration: underline;">Proposing an observatory core modification</span>:
@endsection

@section('titleA')
Info
@endsection
@section('contentA')
You are creating a modification for the core of an observatory.<br/>
<br/>
Current version of the observatory: 
<pre class="json">{{json_encode($dictionary)}}</pre> 
@endsection

@section('titleB')
	Core modification
@endsection
@section('contentB')
	<p>
	    <ul>
	    	<li>
	    		Observatory: <a href="{{route('dictionary.view', $dictionary->id)}}">{{$dictionary->id}}</a>
	    	</li>
	    </ul>
	</p>
	<hr/>	
	{!! Form::open(array('method' => 'PUT', 'route' => array('dictionary.updateCore', $dictionary->id))) !!}
		<input type="hidden" name="dictionaryId" value="{{$dictionary->id}}"/>
		
		<label for="coreId">Core value identifier</label><br/>
		<input type="text" value="{{$coreId}}" disabled/>
		<input type="hidden" name="coreId" value="{{$coreId}}"/>
<br/><br/>
		<label for="coreValue">Value proposition for this core value</label><br/>
		<textarea type="text" name="coreValue" id="coreValue" rows=5 style="width: 100%;">{{$dictionary->$coreId}}</textarea>
@endsection

@section('titleC')
	Action
@endsection
@section('contentC')
	<div class="pure-form">
		<label for="remark">Why do you modify this observatory core value ?</label>
		<textarea name="remark" id="remark" style="width: 100%; min-height: 10em;" required></textarea>
		<br/><br/>
		<input type="submit" name="" class="pure-button pure-button-primary" value="Create proposition !"/>
	</div>
	{!! Form::close() !!}
@endsection


@push('scripts')
<script>
</script>
@endpush

@push('styles')
	<style>
	
	</style>
@endpush
@extends('template-3-columns')

@section('pageTitle')
    Proposing an entry modification
@endsection

@section('title')
    <span style="text-decoration: underline;">Proposing an entry modification</span>:
@endsection

@section('titleB')
	Text
@endsection
@section('contentB')
{!! Form::open(array('method' => 'PUT', 'route' => array('proposition.modifyEntry', $dictionary->id, $entry->hash), 'class' => "pure-form")) !!}
		<label for="value">Entry text (markdown formated)</label>
		<textarea name="value" id="value" style="width: 100%; min-height: 30em;" required>{{$entry->value}}</textarea>
@endsection

@section('titleC')
	Action
@endsection
@section('contentC')
	<div class="pure-form"><label for="tags">Entry tags (comma separated)</label>
		<input type="text" name="tags" id="tags" value="{{$entry->rawTags}}" placeholder="exemple: cowaboo,science,..." style="width: 100%;" required>
		<br/><br/>
		<label for="remark">Why do you modify this entry ?</label>
		<textarea name="remark" id="remark" style="width: 100%; min-height: 10em;" required></textarea>
		<br/><br/>
		<input type="submit" name="" class="pure-button pure-button-primary" value="Create proposition !"/>
	</div>
	{!! Form::close() !!}
@endsection

@section('titleA')
	Search
@endsection
@section('contentA')

<div class="pure-form" id="cowabooSearch">
	<label for="search">Your CoWaBoo search</label>
	<input type="text" name="search" id="search" value="{{$entry->rawTags}}" placeholder="exemple: cowaboo,science,..." style="width: 100%;" required>
	<br/><br/>
	<button type="button" class="pure-button button-success" data-remodal-target="findTags">Find tags</button>
	<button type="button" class="pure-button button-success" data-remodal-target="findBookmarks">Find bookmarks</button>
	<button type="button" class="pure-button button-success" data-remodal-target="findGroups">Find groups</button>
	<button type="button" class="pure-button button-success" data-remodal-target="findUsers">Find users</button>
	<button type="button" class="pure-button button-success" data-remodal-target="findArticles">Find articles</button>
</div>

<div class="remodal pure-form" data-remodal-id="findTags" id="findTags">
	<button data-remodal-action="close" class="remodal-close"></button>
	<h1>Find tags</h1>
	<input type="text" class="form-control tagInput" style="width: 69%;" id="diigoTagGroupSlug" placeholder='Your Diigo group slug, i.e: "e_culture" (optional)'>
	<button type="button" class="pure-button button-success" onclick="launchFindTags();">Relaunch search</button>
	<br/><br/>

	<div style="display: none;" class="modal-result" id="modal-result-tag">
		<label><input type="checkbox" class="" value=""/>
		<span></span></label>
	</div>
	<div class="loading">
		<img src="http://textarea.cowaboo.net/ring.gif" alt="loading...">
	</div>
	<div class="modal-results" id="modal-results-tag">
	</div>
	<br>
	<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
	<button data-remodal-action="confirm" class="remodal-confirm">Insert</button>
</div>

<div class="remodal pure-form" data-remodal-id="findBookmarks" id="findBookmarks">
	<button data-remodal-action="close" class="remodal-close"></button>
	<h1>Find bookmarks</h1>

	<div style="display: none;" class="modal-result" id="modal-result-bookmarks">
		<label><input type="checkbox" class="" value=""/>
		<span></span></label>
	</div>
	<div class="loading">
		<img src="http://textarea.cowaboo.net/ring.gif" alt="loading...">
	</div>
	<div class="modal-results" id="modal-results-bookmarks">
	</div>
	<br>
	<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
	<button data-remodal-action="confirm" class="remodal-confirm">Insert</button>
</div>

<div class="remodal pure-form" data-remodal-id="findGroups" id="findGroups">
	<button data-remodal-action="close" class="remodal-close"></button>
	<h1>Find groups</h1>

	<div style="display: none;" class="modal-result" id="modal-result-groups">
		<label><input type="checkbox" class="" value=""/>
		<span></span></label>
	</div>
	<div class="loading">
		<img src="http://textarea.cowaboo.net/ring.gif" alt="loading...">
	</div>
	<div class="modal-results" id="modal-results-groups">
	</div>
	<br>
	<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
	<button data-remodal-action="confirm" class="remodal-confirm">Insert</button>
</div>

<div class="remodal pure-form" data-remodal-id="findUsers" id="findUsers">
	<button data-remodal-action="close" class="remodal-close"></button>
	<h1>Find users</h1>

	<div style="display: none;" class="modal-result" id="modal-result-users">
		<label><input type="checkbox" class="" value=""/>
		<span></span></label>
	</div>
	<div class="loading">
		<img src="http://textarea.cowaboo.net/ring.gif" alt="loading...">
	</div>
	<div class="modal-results" id="modal-results-users">
	</div>
	<br>
	<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
	<button data-remodal-action="confirm" class="remodal-confirm">Insert</button>
</div>

<div class="remodal pure-form" data-remodal-id="findArticles" id="findArticles">
	<button data-remodal-action="close" class="remodal-close"></button>
	<h1>Find articles</h1>

	<div style="display: none;" class="modal-result" id="modal-result-articles">
		<label><input type="checkbox" class="" value=""/>
		<span></span></label>
	</div>
	<div class="loading">
		<img src="http://textarea.cowaboo.net/ring.gif" alt="loading...">
	</div>
	<div class="modal-results" id="modal-results-articles">
	</div>
	<br>
	<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
	<button data-remodal-action="confirm" class="remodal-confirm">Insert</button>
</div>

@endsection

@push('scripts')
<script type="text/javascript">

$(document).on('opening', '#findTags', function (e,a ,b) {
	if ($('#search').val() != '') {
		launchFindTags();
	}
});

$(document).on('opened', '#findTags', function () {
if ($('#search').val() == '') {
		var inst = $('#findTags').remodal();
		inst.close();
		alertify.error("No search term !");
		return false;
	}
});

$(document).on('confirmation', '#findTags', function () {
	var str = '\n\n###Related tags: \n';

	$('#modal-results-tag').find('input:checked').each(function(key, item) {
		str += '- '+tagFound[$(item).val()]+'\n'; 
	});
	$('#value').val($('#value').val()+str);
});

var tagFound = [];
var launchFindTags = function() {
	$('.loading').show();

	var searchTerm = $('#search').val();
	var url = "http://stadja.net:81/rest/cowaboo/tags/infos?tag_services=diigo&app_name=cowaboo&tag="+searchTerm;

	var diigoTagGroupSlug = $('#diigoTagGroupSlug').val();
	if (diigoTagGroupSlug) {
		url += '&group_slug='+diigoTagGroupSlug;
	}

	$('#modal-results-tag').html('');
	$.ajax({
		url: url,
	}).done(function(data) {
		$(data.diigo).each(function(key, item) {
			tagFound[key] = item;
			if (key > 50) {
				return false;
			}
			var newTagResult = $('#modal-result-tag').clone(true);
			newTagResult.attr('id', '');
			newTagResult.find('span').html(item);
			newTagResult.find('input').val(key);
			newTagResult.show();
			$('#modal-results-tag').append(newTagResult);+
			$('.loading').hide();
		});
	});

	return false; 
}

///////////////////////

$(document).on('opening', '#findBookmarks', function (e,a ,b) {
	if ($('#search').val() != '') {
		launchFindBookmarks();
	}
});

$(document).on('opened', '#findBookmarks', function () {
if ($('#search').val() == '') {
		var inst = $('#findBookmarks').remodal();
		inst.close();
		alertify.error("No search term !");
		return false;
	}
});

$(document).on('confirmation', '#findBookmarks', function () {
	var str = '\n\n###Related bookmarks: \n';
	$('#modal-results-bookmarks').find('input:checked').each(function(key, item) {
		var bookmark = bookmarkFound[$(item).val()];
		str += '- ['+bookmark.title+']('+bookmark.link+')\n'; 
	});
	$('#value').append(str);
});

var bookmarkFound = [];
var launchFindBookmarks = function() {
	$('.loading').show();

	var searchTerm = $('#search').val();
	var url = "http://stadja.net:81/rest/cowaboo/tags/bookmarks?bookmark_services=diigo&app_name=cowaboo&tag="+searchTerm;

	$('#modal-results-bookmarks').html('');
	$.ajax({
		url: url,
	}).done(function(data) {
		bookmarkFound = data.diigo;
		$(data.diigo).each(function(key, item) {
			if (key > 10) {
				return false;
			}
			var newTagResult = $('#modal-result-bookmarks').clone(true);
			newTagResult.attr('id', '');
			newTagResult.find('input').val(key);

			var str = '<a href="'+item.link+'" target="_blank">';
			str += item.title+'</a><br/>';
			str += item.description+'<br/>';
			str += item.pubDate;
			newTagResult.find('span').html(str);

			newTagResult.show();
			$('#modal-results-bookmarks').append(newTagResult);+
			$('.loading').hide();
		});
	});

	return false; 
}

///////////////////////

$(document).on('opening', '#findGroups', function (e,a ,b) {
	if ($('#search').val() != '') {
		launchFindGroups();
	}
});

$(document).on('opened', '#findGroups', function () {
if ($('#search').val() == '') {
		var inst = $('#findGroups').remodal();
		inst.close();
		alertify.error("No search term !");
		return false;
	}
});

$(document).on('confirmation', '#findGroups', function () {
	var str = '\n\n###Related groups: \n';
	$('#modal-results-groups').find('input:checked').each(function(key, item) {
		var group = groupFound[$(item).val()];
		str += '- ['+group.name+']('+group.url+')\n'; 
	});
	$('#value').append(str);
});

var groupFound = [];
var launchFindGroups = function() {
	$('.loading').show();

	var searchTerm = $('#search').val();
	var url = "http://stadja.net:81/rest/cowaboo/tags/groups?group_services=zotero&app_name=cowaboo&tag="+searchTerm;

	$('#modal-results-groups').html('');
	$.ajax({
		url: url,
	}).done(function(data) {
		groupFound = data.zotero;
		$(groupFound).each(function(key, item) {
			if (key > 10) {
				return false;
			}
			var newTagResult = $('#modal-result-groups').clone(true);
			newTagResult.attr('id', '');
			newTagResult.find('input').val(key);

			var str = '<a href="'+item.url+'" target="_blank">';
			str += item.name+'</a><br/>';
			str += item.info+'<br/>';
			newTagResult.find('span').html(str);

			newTagResult.show();
			$('#modal-results-groups').append(newTagResult);+
			$('.loading').hide();
		});
	});

	return false; 
}

///////////////////////

$(document).on('opening', '#findUsers', function (e,a ,b) {
	if ($('#search').val() != '') {
		launchFindUsers();
	}
});

$(document).on('opened', '#findUsers', function () {
if ($('#search').val() == '') {
		var inst = $('#findUsers').remodal();
		inst.close();
		alertify.error("No search term !");
		return false;
	}
});

$(document).on('confirmation', '#findUsers', function () {
	var str = '\n\n###Related users: \n';
	$('#modal-results-users').find('input:checked').each(function(key, item) {
		var data = userFound[$(item).val()];
		str += '- ['+data.name+']('+data.url+')\n'; 
	});
	$('#value').append(str);
});

var userFound = [];
var launchFindUsers = function() {
	$('.loading').show();

	var searchTerm = $('#search').val();
	var url = "http://stadja.net:81/rest/cowaboo/tags/users?user_services=zotero&app_name=cowaboo&tag="+searchTerm;

	$('#modal-results-users').html('');
	$.ajax({
		url: url,
	}).done(function(data) {
		userFound = data.zotero;
		$(userFound).each(function(key, item) {
			if (key > 10) {
				return false;
			}
			var newTagResult = $('#modal-result-users').clone(true);
			newTagResult.attr('id', '');
			newTagResult.find('input').val(key);

			var str = '<a href="'+item.url+'" target="_blank">';
			str += item.name+'</a>';
			str += item.info;
			newTagResult.find('span').html(str);

			newTagResult.show();
			$('#modal-results-users').append(newTagResult);+
			$('.loading').hide();
		});
	});

	return false; 
}

///////////////////////

$(document).on('opening', '#findArticles', function (e,a ,b) {
	if ($('#search').val() != '') {
		launchFindArticles();
	}
});

$(document).on('opened', '#findArticles', function () {
if ($('#search').val() == '') {
		var inst = $('#findArticles').remodal();
		inst.close();
		alertify.error("No search term !");
		return false;
	}
});

$(document).on('confirmation', '#findArticles', function () {
	var str = '\n\n###Related articles: \n';
	$('#modal-results-articles').find('input:checked').each(function(key, item) {
		var data = articleFound[$(item).val()];
		str += '- ['+data.title+']('+data.url+')\n'; 
	});
	$('#value').append(str);
});

var articleFound = [];
var launchFindArticles = function() {
	$('.loading').show();

	var searchTerm = $('#search').val();
	var url = "http://stadja.net:81/rest/cowaboo/tags/infos?tag_services=wikipedia&app_name=cowaboo&tag="+searchTerm;

	$('#modal-results-articles').html('');
	$.ajax({
		url: url,
	}).done(function(data) {
		articleFound = data.wikipedia.articles;
		$(articleFound).each(function(key, item) {
			if (key > 10) {
				return false;
			}
			var newTagResult = $('#modal-result-articles').clone(true);
			newTagResult.attr('id', '');
			newTagResult.find('input').val(key);

			var str = '<a href="'+item.url+'" target="_blank">';
			str += item.title+'</a>';
			str += '<br/><div class="nugget-profile">'+item.snippet+'</div>';
			newTagResult.find('span').html(str);

			newTagResult.show();
			$('#modal-results-articles').append(newTagResult);+
			$('.loading').hide();
		});
	});

	return false; 
}

</script>
@endpush

@push('styles')
	<style>
	.loading {
		display: none;
	}

	#cowabooSearch button {
		margin-bottom: 5px;
	}

	.modal-results {
		display: flex;
		flex-direction: row;
		flex-wrap: wrap;
	}

	.modal-result {
		border: 1px black solid;
		margin: 5px;
		padding: 5px;
    	max-width: 400px;
	}

	.nugget-profile {
		text-align: left;
	}
	@media only screen and (min-width: 641px) {
		.remodal {
		    max-width: 1234px;
		}
	}
	</style>
@endpush
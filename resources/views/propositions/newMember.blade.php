@extends('template-3-columns')

@section('pageTitle')
    Proposing a new member
@endsection

@section('title')
    <span style="text-decoration: underline;">Proposing a new member</span>:
@endsection

@section('titleA')
    Select an entry
@endsection
@section('contentA')
    @include('partials/entryList', array('dictionary' => $dictionary))
@endsection


@section('titleB')
	Become a member
@endsection
@section('contentB')
<p>
	Observatory: <a href="{{route('dictionary.view', $dictionary->id)}}" target="_blank">{{$dictionary->id}}</a>
	{!! Form::open(array('method' => 'PUT', 'route' => array('proposition.newMember', $dictionary->id))) !!}

</p>
@endsection

@section('titleC')
	Action
@endsection
@section('contentC')
<p>
		<input type="hidden" name="dictionaryId" value="{{$dictionary->id}}"/>
		<input type="hidden" name="email" value="{{Auth::user()->email}}"/>
		<br/><br/>
		<label for="value">Presentation</label>
		<textarea name="remark" id="remark" style="width: 100%; min-height: 10em;" required></textarea>
		<input type="submit" name="" value="Create proposition !"/>
	{!! Form::close() !!}
</p>
@endsection

@push('styles')
<style>
    .pure-menu-link, .pure-menu-heading {
        white-space: normal;
    }
</style>
@endpush

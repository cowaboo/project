<!DOCTYPE html>
<html>
    <head>
        <title>
            @section('pageTitle')
                Project
            @show | Cowaboo's protocol
        </title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
                background-image: url({{asset('img/COWABOO_header_double.jpg')}});
            }

            .container {
                vertical-align: middle;
                margin: 15em auto 0 auto;
                width: 90%;
            }

            .content {
                text-align: center;
                display: inline-block;
                background-color: rgba(255,255,255,0.95);
                padding: 0.5em;
                width: 100%;
                font-weight: bold;
                text-align: left;
                float:left;
            }

            .title {
                text-align: center;
                font-size: calc(10vw);
                margin-bottom: 0.5em;
            }

            small {
                font-size: 30%;
            }

            .nav {
                width: 200px;
                float: left;
                padding: 0.5em;
                height: 100%;
                background-color:  rgba(255,255,255,0.95);
                font-weight: bold;
                position: fixed;
            }
            .container {
                margin-left: calc(200px + 1em);
                width: calc(100% - 200px - 2em);
                float: left;
            }

            .nav li a{
                text-decoration: none;
            }

        </style>
    </head>
    <body>
            <div class="nav">
            <ul style="list-style-type:square">
                @if (Auth::user()) 
                    <lo style="list-style-type:none">
                        <li>{{Auth::user()->email}}</li>
                    </lo> 
                    <lo>&nbsp;</lo>
                    <li>
                        <a href="{{route('home')}}">New home</a>
                    </li>
                    <li>
                        <a href="{{route('dictionary.index')}}">Observatory list</a>
                    </li>
                    <li>
                        <a href="{{route('user.index')}}">User list</a>
                    </li>
                    <li>
                        <a href="{{route('template.index')}}">Template list</a>
                    </li>
                    <lo>&nbsp;</lo>
                    <li>
                        <a href="{{route('logout')}}">Logout</a>
                    </li>
                @else
                    <li>
                        <a href="{{route('login')}}">Login</a>
                    </li>
                    <li>
                        <a href="{{route('user.create')}}">Sign in</a>
                    </li>
                @endif
            </ul>
            </div>
        <div class="container">
            <div class="content">
                <div class="title">
                    @section('title')
                        Cowaboo
                    @show
                </div>
                @section('content')
                @show
            </div>
        </div>
        <script   src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>
        <script src="https://cdn.rawgit.com/alertifyjs/alertify.js/v1.0.10/dist/js/alertify.js"></script>
        @stack('scripts')
        <script type="text/javascript">
            @if (Session::has('error'))
                alertify.error("{{Session::get('error')}}");
            @endif
            @if (Session::has('success'))
                alertify.success("{{Session::get('success')}}");
            @endif
        </script>
    </body>
</html>

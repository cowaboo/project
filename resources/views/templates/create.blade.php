@extends('template')

@section('pageTitle')
    New template creation
@endsection

@section('title')
    <span style="text-decoration: underline;">Creation of a new template</span>:
@endsection

@section('content')
	{!! Form::open(array('method' => 'POST', 'route' => array('template.store'))) !!}
		@include('templates.form', array('edit' => false))
		<br/>
		<input type="submit" name="" value="Create a new template !"/>
	{!! Form::close() !!}
@endsection
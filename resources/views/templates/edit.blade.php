@extends('template')

@section('pageTitle')
    {{$template->id}} template modification
@endsection

@section('title')
    <span style="text-decoration: underline;">{{$template->id}} template modification</span>:
@endsection

@section('content')
	{!! Form::model($template, array('method' => 'PUT', 'route' => array('template.update', $template->id))) !!}
		@include('templates.form', array('edit' => true, 'template' => $template))
		<br/>
		<input type="submit" name="" value="Modify template !"/>
	{!! Form::close() !!}
@endsection
<label for="id">Template identifier </label>
@if ($edit)
	<input type="text" name="id" id="id" value="{{$template->id}}" placeholder="" style="width: 99%;" required>
@else
	<input type="text" name="id" id="id" value="" placeholder="" style="width: 99%;" required>
@endif
<br/><br/>

<div style="width: 99%; text-align: center;">
	<a href="#" onclick="addNewEntryInputs(); return false;">+ add a new mandatory entry</a>
</div>

<div style="display: none;" id="entryExample">
	<div style="width: 10%; display: inline-block;">
		&nbsp;
	</div>
	<label for="entries[test]">Mandatory entry 1</label><br/>
	<div style="width: 10%; display: inline-block; text-align: center;">
		<a href="#" onclick="$(this).parent().parent().remove(); return false;";>Remove</a>
	</div>
	<input class="display: inline-block;" type="text" value="" placeholder="exemple: title, subtitle, blog ..." style="width: 89%;">
	<br/><br/>
</div>

<div id="entryList">
	@if ($edit)
		@foreach ($template->entries as $key => $entry)
			<div class="entryName">
				<div style="width: 10%; display: inline-block;">
					&nbsp;
				</div>
				<label for="entries[{{$key}}]">Mandatory entry {{$key + 1}}</label><br/>
				<div style="width: 10%; display: inline-block; text-align: center;">
					<a href="#" onclick="$(this).parent().parent().remove(); return false;";>Remove</a>
				</div>
				<input type="text" name="entries[{{$key}}]" id="entries[{{$key}}]" value="{{$entry}}" placeholder="exemple: title, subtitle, blog ..." style="width: 89%;" required>
				<br/><br/>
			</div>
		@endforeach
	@else
		<div class="entryName">
			<div style="width: 10%; display: inline-block;">
				&nbsp;
			</div>
			<label for="entries[0]">Mandatory entry 1</label><br/><br/>
			<div style="width: 10%; display: inline-block;">
				&nbsp;
			</div>
			<input type="text" name="entries[0]" id="entries[0]" value="" placeholder="exemple: title, subtitle, blog ..." style="width: 89%;" required>
			<br/><br/>
		</div>
	@endif
</div>

<label for="applicationUrl">Application url</label> - application url to which we can add the observatory id
@if ($edit)
	<input type="text" value="{{$template->applicationUrl}}" id="applicationUrl" name="applicationUrl" placeholder="exemple: http://project.cowaboo.net/blog/demo/" style="width: 99%;">
@else
	<input type="text" value="" id="applicationUrl" name="applicationUrl" placeholder="exemple: http://project.cowaboo.net/blog/demo/" style="width: 99%;">
@endif

<br/><br/>
<label for="explaination">Explaination</label>
@if ($edit)
	<textarea name="explaination" id="explaination" style="width: 99%;" rows=5 required>{{$template->explaination}}</textarea>
@else
	<textarea name="explaination" id="explaination" style="width: 99%;" rows=5 required></textarea>
@endif

<br/>

@push('scripts')
<script type="text/javascript">
var nbrOfEntry = $('.entryName').length;
var addNewEntryInputs = function() {
	var newEntry = $('#entryExample').clone(true);
	newEntry.attr('id', '');
	newEntry.addClass('entryName');
	var label = $(newEntry.find('label')[0]);
	label.attr('for', 'entries['+nbrOfEntry+']');
	label.html('Mandatory entry '+(nbrOfEntry+1));
	var input = $(newEntry.find('input')[0]);
	input.attr('id', 'entries['+nbrOfEntry+']');
	input.attr('name', 'entries['+nbrOfEntry+']');
	input.prop('required', true);

	newEntry.show();

	$('#entryList').append(newEntry);
	nbrOfEntry++;
	return false;
}
</script>
@endpush
@extends('template')

@section('pageTitle')
    New template creation
@endsection

@section('title')
    <span style="text-decoration: underline;">Creation of a new template</span>:
@endsection

@section('content')
	{!! Form::open(array('method' => 'POST', 'route' => array('template.store'))) !!}
		<label for="id">Template identifier </label>
		<input type="text" name="id" id="id" value="" placeholder="" style="width: 100%;" required>
		<br/><br/>

		<div style="width: 100%; text-align: center;">
			<a href="#" onclick="addNewEntryInputs(); return false;">+ add a new mandatory entry</a>
		</div>

		<div style="display: none;" id="entryExample">
			<div style="width: 10%; display: inline-block;">
				&nbsp;
			</div>
			<label for="entries[test]">Mandatory entry 1</label><br/>
			<div style="width: 10%; display: inline-block; text-align: center;">
				<a href="#" onclick="$(this).parent().parent().remove(); return false;";>Remove</a>
			</div>
			<input class="display: inline-block;" type="text" value="" placeholder="exemple: title, subtitle, blog ..." style="width: 89%;">
			<br/><br/>
		</div>

		<div id="entryList">
			<div class="entryName">
				<div style="width: 10%; display: inline-block;">
					&nbsp;
				</div>
				<label for="entries[0]">Mandatory entry 1</label><br/><br/>
				<div style="width: 10%; display: inline-block;">
					&nbsp;
				</div>
				<input type="text" name="entries[0]" id="entries[0]" value="" placeholder="exemple: title, subtitle, blog ..." style="width: 89%;" required>
				<br/><br/>
			</div>
		</div>

		<br/><br/>
		<input type="submit" name="" value="Create a new template !"/>
	{!! Form::close() !!}
@endsection

@push('scripts')
<script type="text/javascript">
var nbrOfEntry = $('.entryName').length;
var addNewEntryInputs = function() {
	var newEntry = $('#entryExample').clone(true);
	newEntry.attr('id', '');
	newEntry.addClass('entryName');
	var label = $(newEntry.find('label')[0]);
	label.attr('for', 'entries['+nbrOfEntry+']');
	label.html('Mandatory entry '+(nbrOfEntry+1));
	var input = $(newEntry.find('input')[0]);
	input.attr('id', 'entries['+nbrOfEntry+']');
	input.attr('name', 'entries['+nbrOfEntry+']');
	input.prop('required', true);

	newEntry.show();

	$('#entryList').append(newEntry);
	nbrOfEntry++;
	return false;
}
</script>
@endpush
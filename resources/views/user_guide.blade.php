@extends('template-3-columns')

@section('pageTitle')
    User guide
@endsection

@section('title')
    <span style="text-decoration: underline;">{{CowabooProject::findEntry('user_guide_title')->value}}</span>
@endsection

@section('titleA')
{{CowabooProject::findEntry('user_guide_left_title')->value}}
@endsection
@section('contentA')
{!!CowabooProject::findEntry('user_guide_left_text')->value_html!!}
@endsection

@section('titleB')
{{CowabooProject::findEntry('user_guide_center_title')->value}}
@endsection
@section('contentB')
{!!CowabooProject::findEntry('user_guide_center_text')->value_html!!}
@endsection

@section('titleC')
{{CowabooProject::findEntry('user_guide_right_title')->value}}
@endsection
@section('contentC')
{!!CowabooProject::findEntry('user_guide_right_text')->value_html!!}
@endsection
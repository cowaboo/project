@extends('template')

@section('pageTitle')
    User list
@endsection

@section('title')
    <span style="text-decoration: underline;">User list</span> <small>(current&nbsp;version)</small>
@endsection

@section('content')
<!-- <p style="text-align: center;">
	<a href="{{route('user.create')}}">+ create a new user</a>
</p> -->

<dl>
    @foreach ($userList->list as $email => $hash)
      <dt>{{$email}}</dt>
	      <dd>Public Address: {{$hash}}</dd>
    @endforeach
</dl>

<p>
    <a href="https://gateway.ipfs.io/ipfs/{{$userList->hash}}" target="_blank">#</a>
</p>

<p>
@if ($userList->previous)
    <a href="{{route('viewer.view', $userList->previous)}}">Previous version</a>
@else
First version
@endif
</p>

@endsection
@extends('template')

@section('pageTitle')
    Create a new user
@endsection

@section('title')
	Create a new user
@endsection

@section('content')
	{!! Form::open(array('method' => 'POST', 'route' => array('user.create'))) !!}
		<label for="email">Your email</label>
		<input type="email" name="email" id="email" value="" placeholder="" style="width: 100%;" required>
		<br/><br/>
		<input type="submit" name="" value="Create a new user !"/>
	{!! Form::close() !!}
@endsection
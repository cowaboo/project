@extends('template-3-columns')

@section('pageTitle')
    New user accepted
@endsection

@section('title')
	New user creation !
@endsection

@section('titleA')
	Welcome !
@endsection
@section('contentA')
	You can now access the CoWaBoo application
@endsection

@section('titleB')
	Very Important Info
@endsection
@section('contentB')
	{!!str_replace(
		array('%EMAIL%'), 
		array($email), 
		CowabooProject::findEntry('CoWaBoo,Welcome,text')->value_html
	)!!}
@endsection

@section('titleC')
	Login
@endsection
@section('contentC')
	{!! Form::open(array('method' => 'POST', 'route' => array('login'), 'class' => "pure-form")) !!}
		<label for="secretKey">Enter your secret key</label>
		<input type="password" name="secretKey" id="secretKey" value="" placeholder="" style="width: 100%;" required>
		<br/><br/>
		<input type="submit" name="" value="Login and welcome !" class="pure-button pure-button-primary"/>
	{!! Form::close() !!}
@endsection
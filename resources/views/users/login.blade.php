@extends('template-3-columns')

@section('pageTitle')
    CoWaBoo
@endsection


@section('titleA')
	Presentation
@endsection
@section('contentA')
	{!!CowabooProject::findEntry('login_presentation_text')->value_html!!}
@endsection

@section('titleB')
	Login
@endsection
@section('contentB')
{!! Form::open(array('method' => 'POST', 'route' => array('login'), 'class' => "pure-form")) !!}
	<label for="secretKey">Your secret key</label>
	<input type="password" name="secretKey" id="secretKey" value="" placeholder="" style="width: 100%;" required>
	<br/><br/>
	<input type="submit" name="" value="Glad to have you back !" class="pure-button pure-button-primary"/>
{!! Form::close() !!}
@endsection

@section('titleC')
	Signup
@endsection
@section('contentC')
	{!! Form::open(array('method' => 'POST', 'route' => array('user.create'), 'class' => "pure-form")) !!}
		<label for="email">Your email</label>
		<input type="email" name="email" id="email" value="" placeholder="" style="width: 100%;" required>
		<br/><br/>
		<input type="submit" name="" value="Create a new user" class="pure-button pure-button-primary"/>
	{!! Form::close() !!}
@endsection
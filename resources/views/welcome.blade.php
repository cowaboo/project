@extends('template-3-columns')

@section('pageTitle')
    CoWaBoo
@endsection


@section('titleA')
	All your observatory tags
@endsection
@section('contentA')

<input type="text" v-model="userObservatoryFilter" id="userObservatoryFilter" value="" placeholder="Filter observatories by tag" style="width: 100%;" required=""><br/>&nbsp;

<dl>
    <li v-for="obs in filteredUserObservatories" class="pure-menu-item">
        <dt><strong><a v-bind:href="obs.url">@{{obs.id | html_entity_decode}}</a></strong></dt>
        <dd v-if="obs.data && !userObservatoryFilter">
            ||
            <span v-for="tag in obs.data"><span v-on:mouseover="relativeObservatoryFilter = $('<textarea />').html(tag.tag).text(); userDown();" v-on:mouseout="userUp();" class="userObservatory tag button-neutral" v-bind:data-tag="tag.strtolower"><a v-bind:href="tag.url">@{{tag.tag | html_entity_decode}}</a></span> ||</span>
        </dd>
    </li>
</dl>


@endsection

@section('titleB')
	Relative observatories <a href="/ipfs/{{ $tagList->hash }}" target="_blank">#</a>
@endsection
@section('contentB')
<input type="text" v-model="relativeObservatoryFilter" id="relativeObservatoryFilter" value="" placeholder="Filter observatories by tag" style="width: 100%;" required=""><br/>&nbsp;

<dl>
    <li v-for="obs in filteredRelativeObservatories" class="pure-menu-item">
        <dt><strong><a v-bind:href="obs.url">@{{obs.id | html_entity_decode}}</a></strong></dt>
        <dd v-if="obs.data && !relativeObservatoryFilter">
            ||
            <span v-for="tag in obs.data"><span v-on:mouseover="userObservatoryFilter = $('<textarea />').html(tag.tag).text(); relativeDown();" v-on:mouseout="relativeUp();" class="relativeObservatory tag button-neutral" v-bind:data-tag="tag.strtolower"><a v-bind:href="tag.url">@{{tag.tag | html_entity_decode}}</a></span> ||</span>
        </dd>
    </li>
</dl>
@endsection

@section('titleC')
	Create a new observatory
@endsection
@section('contentC')
	{!! Form::open(array('method' => 'POST', 'route' => array('dictionary.createNew'), 'class' => "pure-form")) !!}
		<label for="id">Observatory identifier </label>
		<input type="text" name="id" id="id" value="" placeholder="exemple: cowaboo,science,..." style="width: 100%;" required>
		<br/><br/>
		<label for="id">Template to use </label><br/>
		<select name="template">
			<option value="">None</option>
			@foreach ($templateList->list as $templateId => $template)
				<option value="{{$templateId}}">{{$templateId}}</option>
			@endforeach
		</select>
		<br/><br/>
		<input class="pure-button pure-button-primary" type="submit" name="" value="Create this observatory !"/>
	{!! Form::close() !!}
    <br/>
    Main discussion
    <div id="disqus_thread"></div>
@endsection

@push('scripts')
<script>
    /**
     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
     */

    var disqus_config = function () {
        this.page.url = "{{ route('home') }}";  // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = "home"; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };

    (function() {  // DON'T EDIT BELOW THIS LINE
        var d = document, s = d.createElement('script');

        s.src = '//cowaboo.disqus.com/embed.js';

        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>

<script>

    var relativeObservatories = [
    @foreach (Auth::user()->other_dictionaries as $id => $tags)
    {
        id: "{{$id}}",
        url: "{{route('dictionary.view', $id)}}",
        data: [
            @foreach (array_filter(array_unique(explode('||', $tags))) as $tag)
            {
                "tag": "{{$tag}}",
                "strtolower": "{{strtolower($tag)}}",
                "url": "{{ route('tag.info', urlencode(strtolower($tag))) }}",
            },
            @endforeach
        ],
    },
    @endforeach
    ];
    var userObservatories = [
    @foreach (Auth::user()->dictionaries as $id => $tags)
    {
        id: "{{$id}}",
        url: "{{route('dictionary.view', $id)}}",
        data: [
            @foreach (array_filter(array_unique(explode('||', $tags))) as $tag)
            {
                "tag": "{{$tag}}",
                "strtolower": "{{strtolower($tag)}}",
                "url": "{{ route('tag.info', urlencode(strtolower($tag))) }}",
            },
            @endforeach
        ],
    },
    @endforeach
    ];

    var entryFilter = new Vue({
        el: '#mainRow',
        data: {
            relativeObservatories: relativeObservatories,
            filteredRelativeObservatories: relativeObservatories,
            relativeObservatoryFilter: "",
            userObservatories: userObservatories,
            filteredUserObservatories: userObservatories,
            userObservatoryFilter: "",
            userTimeout: false,
            relativeTimeout: false
        },
        filters: {
            html_entity_decode: function (value) {
                if (!value) return ''
                value = value.toString()
                return $('<textarea />').html(value).text();
            }
        },
        watch: {
            relativeObservatoryFilter: function (newFilter) {
                this.filteredRelativeObservatories = this.filterObservatories(this.relativeObservatories, newFilter);
            },
            userObservatoryFilter: function (newFilter) {
                this.filteredUserObservatories = this.filterObservatories(this.userObservatories, newFilter);
            }
        },
        methods: {
            filterObservatories: function(observatories, filterString) {
                if(!filterString){
                    return observatories;
                }
                filterString = no_accent(filterString.trim().toLowerCase());
                result = observatories.filter(function(observatory){
                    var tags = observatory.data;
                    var isMatching = false;
                    for (var i = 0; i < tags.length; i++) {
                        var testedTag = $('<textarea />').html(tags[i].tag).text();
                        var testedTag = no_accent(testedTag.toLowerCase());
                        if(testedTag.indexOf(filterString) !== -1){
                            isMatching = true;
                            break;
                        }
                    }
                    return isMatching;
                });
                return result;
            },
            clearRelativeFilter: function() {
                this.relativeObservatoryFilter = '';
            },
            clearUserFilter: function() {
                this.userObservatoryFilter = '';
            },
            userDown: function() {
                clearTimeout(this.userTimeout);
            },
            userUp: function() {
                this.userTimeout = setTimeout(this.clearRelativeFilter, 1000);
            },
            relativeDown: function() {
                clearTimeout(this.relativeTimeout);
            },
            relativeUp: function() {
                this.relativeTimeout = setTimeout(this.clearUserFilter, 1000);
            }
        }
    });

</script>
@endpush

@push('styles')
<style>
	.tag a {
		text-decoration: none;
	}
</style>
@endpush
